<?php


use Illuminate\Support\Str;

Route::get('/', function () {
    return view('main');
});

// Route::get('/playdemo', function () {
//     return view('demo');
// });
Route::get('/vendors', function () {
    $vendors = App\Vendor::orderBy('name', 'Desc');
    return view('vendors', [
        'vendors' => $vendors
    ]);
});
Route::get('/terms', function () {
    return view('terms');
})->name('terms');

Route::get('/how', function () {
    return view('how');
})->name('how');

Route::get('/policy', function () {
    return view('policy');
})->name('policy');

Route::get('/faq', function () {
    return view('faq');
})->name('faq');


Route::get('/playdemo', 'GameController@demo');
Route::post('record-session', 'GameController@submitSession');

Auth::routes();



Route::middleware(['auth'])->group(function() {
    Route::get('/account', 'HomeController@index')->name('dashboard');
    
    Route::get('/leaderboard', 'UsersPagesController@board')->name('board');

    Route::get('/settings', 'UsersPagesController@settings')->name('settings');

    Route::post('/settings/update', 'UsersPagesController@updateSettings')->name('update-settings');

    Route::get('/task', 'TaskController@index')->name('task.index');

    Route::get('/earnings','EarningController@index')->name('earning.index');


    Route::post('/record-session-live', 'GameController@submitLiveSession');
    

    //Update facebooklink
    Route::post('/update/fb-link', 'SocialController@update')->name('update.fb');

    //Move Referral Bonus to wallet
    Route::get('/funds/move', 'WalletController@moveReferralBonus')->name('moveTowallet');

    //Add Banking Details
    Route::post('/bank-details/add', 'EarningController@storeBankDetails')->name('storeBankDetails');

    //Withdrawal Request 
    Route::get('/request/withdraw', 'EarningController@withdrawRequest')->name('request-withdraw');
    
    //Typing
    Route::get('/typing', 'TypingController@start')->name('typing.start');
    Route::post('/submit/typing', 'TypingController@submit')->name('typing.submit');

    Route::get('/task/complete', 'SocialController@completeTask')->name('task.complete');
    Route::post('/task/complete', 'SocialController@submitTask')->name('task.submit');

    //Marking tasks done and undone
    Route::get('/task/{id}/mark-done', 'SocialController@taskDone')->name('task.markdone');
    Route::get('/task/{id}/mark-undone', 'SocialController@taskUndone')->name('task.markundone');

     // Added by robert
    Route::post('/process-withdrawal', 'EarningController@withdrawToBank');
});




//Management Area

Route::group([ 'middleware' => ['is_admin'], 'prefix' => 'manage'], function () {
    //Dashboard
    Route::get('/', 'Management\PagesController@index')->name('management.dashboard');
    
    //Coupons
    Route::get('/coupons', 'CouponController@index')->name('management.coupon.index');
    Route::get('/coupon/create', 'CouponController@create')->name('management.coupon.create');
    Route::post('/coupon/create', 'CouponController@store')->name('management.coupon.store');
    Route::get('/coupons/show', 'CouponController@show')->name('management.coupon.show');

    // vendors
    Route::get('/vendors', 'Management\VendorController@index')->name('management.vendors');
    Route::get('/add/vendors', 'Management\VendorController@create')->name('management.vendors.add');
    Route::post('/vendors/add', 'Management\VendorController@store')->name('management.vendors.store');

    //Typing
    Route::get('/typing', 'Management\TypingController@addSession')->name('management.typing.add');
    Route::get('/typing-session', 'Management\TypingController@index')->name('management.typing.index');
    Route::post('/typing-session/store', 'Management\TypingController@store')->name('management.typing.store');

    //Funding
    Route::get('/funding', 'Management\FundingController@index')->name('management.funding');
    Route::get('/funding/add', 'Management\FundingController@add')->name('management.addfunds');
    Route::post('/funding/addfunds', 'Management\FundingController@addfunds')->name('management.fund');
    Route::get('/payout/{id}', 'Management\FundingController@payOut')->name('management.payOut');
    Route::get('/payout', 'Management\FundingController@payOut')->name('management.payOut.all');

    //WithdrawalRequest
    Route::get('/withdrawal/requests', 'Management\PagesController@withdrawals')->name('management.withdrawals');

    //Leader Board
    Route::get('/leader-board', 'Management\PagesController@leader')->name('management.lead');

    // Users
    Route::get('/users', 'Management\PagesController@users')->name('management.users');

    //security
    Route::get('/password', 'Management\SecurityController@index')->name('management.password');
    Route::post('/password.reset', 'Management\SecurityController@resetPassword')->name('management.password.reset');
    
});