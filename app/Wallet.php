<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = [
        'user_id',
        'referral_bonus',
        'earnings',
        'wallet_balance',
        'pending_balance',
        'daily_earnings',
        'total',
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
