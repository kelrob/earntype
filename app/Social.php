<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $fillable = [
        'user_id',
        'fb_link',
        'total_task',
        'task_completed',
        'task_undone',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
