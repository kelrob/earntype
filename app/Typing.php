<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typing extends Model
{
    protected $fillable = [
        'user_id',
        'username',
        'sec',
        'errors',
        'correct',
        'words_typed',
        'skipped_words',
        'session',
        'level',
        'position',
    ];

    public function typing_session()
    {
        return $this->belongsTo('App\Session');
    }

    
}
