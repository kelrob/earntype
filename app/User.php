<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
         'email', 
         'password',
         'referral_token', 
         'referrer_id', 
         'coupon_code',
         'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['referral_link'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id', 'id');
    }

    public function referrals()
    {
        return $this->hasMany(User::class, 'referrer_id', 'id');
    }

    public function getReferralLinkAttribute()
    {
        return $this->referral_link = route('register', ['ref' => $this->referrer_token]);
    }

    public function bank()
    {
        return $this->hasOne('App\Banking');
    }

    public function wallet()
    {
        return $this->hasOne('App\Wallet');
    }

    public function withdraw()
    {
        return $this->hasMany('App\Withdraw');
    }

    public function social()
    {
        return $this->hasOne('App\Social');
    }

    public function social_task()
    {
        return $this->hasMany('App\STask');
    }


}
