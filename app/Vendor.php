<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = [
        'name',
        'email',
        'contact_no',
        'bank_name',
        'account_name',
        'account_number',
    ];


    public function coupons()
    {
        return $this->hasMany('App\Coupon');
    }
}
