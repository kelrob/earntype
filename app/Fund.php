<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
     protected $fillable = [
       'amount',
       'days',
       'value_per_day',
       'value_per_session',
       'users_to_award',
       'fund_remains',
       'end_date',

       
    ];

}