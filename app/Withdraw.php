<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $fillable = [
        'user_id',
        'amount',
        'account_number',
        'account_name',
        'bank_name',
        'bank_code',
        'status',
        'txn_ref'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
