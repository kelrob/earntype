<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Coupon;
use App\Wallet;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo(){
        return '/account';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function showRegistrationForm(Request $request)
    {
        if ($request->has('ref')) {
            session(['referrer' => $request->query('ref')]);
        }
        return view('auth.register');
    }
    

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'coupon' => ['required', 'string', 'max:255', 'unique:users,coupon_code'],
            'password' => ['required', 'string', 'min:8'],
            'agree' => ['required', 'boolean'],
        ], [
            'coupon.uuid' => 'invalid coupon code',
            'agree.required' => 'you should agree to our terms and conditions'
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $coupon = Coupon::where('code', $data['coupon'])->first();
        if($coupon == true){
            $coupon->update([
                'status' => 'used'
            ]);
            $coupon_code = $coupon->code;

             $referrer = User::whereReferral_token(session()->pull('referrer'))->first();
             
                if($referrer == true){
                    $amount = 1000;
                    $wallet = $referrer->wallet;
                    $referrer_bonus = $wallet->referral_bonus;
                    $wallet->update([
                        'referral_bonus' => $referrer_bonus + $amount,
                        'pending_balance' => $wallet->pending_balance + $amount,
                        'total' => $wallet->total + $amount
                    ]);
                }


                $user =  User::create([
                    'username' => $data['username'],
                    'referrer_id' => $referrer ? $referrer->id : null,
                    'referral_token' => Str::random(8),
                    'email' => $data['email'],
                    'coupon_code' => $coupon_code,
                    'password' => Hash::make($data['password']),
                    'is_admin' => false
                ]);

                $wallet = Wallet::create([
                    'user_id' => $user->id,
                    'wallet_balance' => 0.00,
                    'total' => 0.00,
                ]);

            return $user;
        }else{
            session()->flash('error', 'Invalid Coupon Code');
            die('Invalid Coupon Code');
        }

       

        

    }
}
