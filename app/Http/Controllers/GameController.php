<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Text;
use App\Session;
use App\SessionRecord;
use App\Wallet;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
  public function demo() {
    
    $game = Text::inRandomOrder()->limit(1)->first();
    return view('demo', compact('game'));
  }

  public function submitSession(Request $request) {
    $textEntered = trim($request->textEntered);
    $actualText = trim($request->actualText);

    if (strcmp($textEntered, $actualText) == 0) {

      // $record = new SessionRecord();
      // $record->start_time = $request->token_t;
      // $record->end_time = time();
      // $record->save();

      return 'correct';
    } else {
      return 'wrong';
    }
  }

  public function submitLiveSession(Request $request) {
    $textEntered = trim($request->textEntered);
    $actualText = trim($request->actualText);

    if (strcmp($textEntered, $actualText) == 0) {
      $user = Auth::user();

      $record = new SessionRecord();
      $record->user_id = $user->id;
      $record->start_time = $request->token_t;
      $record->end_time = time();
      $record->time_spent = $request->t_s;
      $record->save();

      $userWallet = Wallet::where('user_id', $user->id)->first();
      $userWallet->daily_earnings = $userWallet->daily_earnings + 50;
      $userWallet->save();
      

      return 'correct';
    } else {
      return 'wrong';
    }
  }
}
