<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
   public function banks()
   {
       $key = env('ATG_PRIVATE_KEY');
       $client = new \GuzzleHttp\Client();
       $banks = $client->request( 'GET', 'https://aimtoget.com/api/v1/', ['SECRET_KEY' => $key]);
        dd($key);
   }
}
