<?php

namespace App\Http\Controllers;

use App\Typing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersPagesController extends Controller
{
    public function board()
    {
        $typing = Typing::orderBy('position', 'Desc')->paginate(25);
        return view('user.board', [
            'typings' => $typing
        ]);
    }
    public function settings()
    {
        $user = Auth::user();
        
        return view('user.settings',[
            'user' => $user,
        ]);
    }

    public function updateSettings(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string|max:50|unique:users,username',
            'email' => 'required|string|max:50|email|unique:users,email'
        ]);

        $username = $request->username;
        $email = $request->email;

        $user = Auth::user();
        $user->update([
            'username' => $username,
            'email' => $email,
        ]);

        session()->flash('success', 'Account Updated Successfully');
        return redirect()->back();
    }


    public function saveBankDetails(Request $request)
    {
        $this->validate($request, [
            'bank'=> 'required|string|max:50',
            'account_name' => 'required|string|max:100',
            'account_number' => 'required|numeric'
        ]);

        $user = Auth::user();
        $bcode = '';
        $banking = Banking::create([
            'bank' => $request->bank,
            'account_name' => $request->account_name,
            'account_number' => $request->account_number,
            'bank_code' => $bcode
        ]);

        \session()->flash('success', 'Banking Details Added Successfully');
        return redirect()->back();
    }


}
