<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Vendor;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::orderBy('created_at', 'DESC')->paginate(25);
        
        return view('management.coupon.index', [
            'coupons' => $coupons
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('management.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'vendor' => 'nullable|string|max:50',
            'amount' => 'required|integer'
        ]);

        $vendor = Vendor::where('id', $request->vendor)->first();
        
        for($i =0; $i <= $request->amount; $i++){
            $coupon = Coupon::create([
                'vendor_id'=> $vendor->id,
                'code' => Uuid::generate(),
                'given_to' => $vendor->name
            ]);

            $coupons[] = $coupon->code;
        }
        session()->flash('success', 'Coupons Generated Successfully');
        // return view('management.coupon.show',[
        //     'coupons' => $coupons
        // ]);

        return view('management.coupon.show', compact('coupons'));

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('management.coupon.show');
    }

  
}