<?php

namespace App\Http\Controllers;

use App\Banking;
use App\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EarningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $withdrawal  = $user->withdraw;
        return view('user.earning.index',[
            'withdraws'=> $withdrawal
        ]);
    }

    
    public function create()
    {
        //
    }

    public function storeBankDetails(Request $request)
    {
       $this->validate($request, [
           'bankName' => 'required',
           'accountName' => 'required|string|max:100',
           'accountNumber' => 'required|numeric|min:10'
       ]);

       $user = Auth::user();

       $bank = explode(",", $request->bankName);

       $bankName = $bank[0];
       $bankCode = $bank[1];

       $banking = Banking::create([
           'user_id' => $user->id,
           'bank_name' => $bankName,
           'bank_code' => $bankCode,
           'account_name' => $request->accountName,
           'account_number' => $request->accountNumber,
       ]);

       session()->flash('success', 'Banking Details Added Successfully');
       return redirect()->back();
    }


    public function withdrawRequest()
    {
        $user = Auth::user();
        $bankDetails = $user->bank;
        $wallet = $user->wallet;

        $latest = Withdraw::where('user_id', $user->id)->where('status', true)->first();
        if($latest == true && $latest->status == false){
            session()->flash('error', 'You already have a pending withdrawal request');
        }else{
            $withdraw = Withdraw::create([
                'user_id' => $user->id,
                'amount' => $wallet->wallet_balance,
                'bank_name' => $bankDetails->bank_name,
                'account_name' => $bankDetails->account_name,
                'account_number' => strval($bankDetails->account_number),
                'bank_code' => strval($bankDetails->bank_code),
                'status' => false
            ]);
            session()->flash('success', 'Your request to withdraw was sent successfully');
        }


        return redirect()->back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function withdrawToBank(Request $request) {
    
        $user = Auth::user();
        $userID = $user->id;

        # Validation
        $validateData = Validator::make($request->all(),
            [
                'amount' => 'required',
                'accountName' => 'required',
                'accountNumber' => 'required|min:10',
                'bankName' => 'required',
            ]
        );

        # If Validation fails
        if ($validateData->fails()) {
            return Redirect::back()->withInput()->withErrors($validateData->messages());
        }

        // $bank = Banking::create([
        //     'bank' => $request->bankName,
        //     'bank_code' => $request->bankName,
        //     'account_number' => $request->accountNumber,
        //     'account_name' => $request->accountName,
        // ]);

        $amount = $request->amount;
        $bankCode = $request->bankName;
        $accountNumber = $request->accountNumber;
        $description = 'EarnType';

        $userWallet = $user->wallet->wallet_balance;
        $newWallet = $userWallet - $amount;
        

        if ($userWallet >= $amount) {
            $post = array(
                'amount' => $amount, 
                'bank_code' => $bankCode, 
                'account_number' => $accountNumber, 
                'description' => $description,
                'pin' => "1234"
            );

            $wallet = Wallet::where('user_id', $userID)->first();
            $wallet->wallet_balance = $newWallet;
            $wallet->save();
    
            $post = json_encode($post);
            $authorization = "Authorization: Bearer d48b0e8251db0dcbd90ce9a04fc64e6652664fa96d6f954974d8c122efc2d293";
    
            $ch = curl_init('https://aimtoget.com/api/v1/bank-transfer');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    
            // execute!
            $response = curl_exec($ch);
    
            // close the connection, release resources used
            curl_close($ch);
    
            $response = json_decode($response);
            $message = $response->msg;
    
            Session::flash('message', $message);
            return Redirect::back();
        } else {
            return Redirect::back()->withErrors(['Insufficient funds in wallet', 'The Message']);
        }
    
    }
}
