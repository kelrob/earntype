<?php

namespace App\Http\Controllers;

use App\STask;
use App\Social;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $social = Social::where('user_id', $user->id)->first();
       
        if($social == true){
            
            $link = $social->fb_link;
        }else{
            $link = 'https://facebook.com';
        }
       
        $tasks  = STask::whereDate('created_at', '=', Carbon::today()->toDateString())->where('user_id', '!=', $user->id)
                ->paginate(20);
        $myTask =  $user->social_task;
        return view('user.task.index', [
            'tasks' => $tasks,
            'link' => $link,
            'myTasks' => $myTask
        ]);
    }


    
}
