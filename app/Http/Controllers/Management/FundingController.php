<?php

namespace App\Http\Controllers\Management;

use App\Fund;
use App\User;
use App\Wallet;
use App\Withdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FundingController extends Controller
{
    public function index()
    {
        $now = now();
        $fund = Fund::whereDate('end_date', '>=' , $now)->first();
        return view('management.funding.index',[
            'funds' => $fund
        ]);
    }


    public function add()
    {
        return view('management.funding.add');
    }


    public function addfunds(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required|numeric',
            'daysfor' => 'required|integer',
        ]);
        
        $days = $request->daysfor;
        $amount = $request->amount;

        $value_per_day = floor($amount / $days);//Get Vales Per day
        $value_per_session = floor($value_per_day / 3); // Get Value per session
        $user_to_award = floor($value_per_session / 500); // Get Values per user

        $endDate = Carbon::now()->addDays($days)->format('Y-m-d');// Funding End Date

        $now = now();
        $funds = Fund::whereDate('end_date', '>=' , $now)->first();

        if($funds == true){
            $funds->update([
                'amount' => $request->amount,
                'days' => $days,
                'value_per_day' => $value_per_day,
                'value_per_session' => $value_per_session,
                'users_to_award' => $user_to_award,
                'end_date' => $endDate,
            ]);
        }else{
            $fund  = Fund::create([
                'amount' => $request->amount,
                'days' => $days,
                'value_per_day' => $value_per_day,
                'value_per_session' => $value_per_session,
                'users_to_award' => $user_to_award,
                'end_date' => $endDate,
            ]);
        }


        session()->flash('success', 'Funds Added Successfully');
        return \redirect()->route('management.funding');
        
    }

    public function payOut($id)
    {


      $user = User::find($id);
      $amount = $user->withdraw[0]->amount;
      $bankCode = $user->withdraw[0]->bank_code;
      $accountNumber = $user->withdraw[0]->account_number;
      $description = 'EarnType';
      $payUser = $amount - 100;
      
    
      $post = array(
        'amount' => strval($payUser), 
        'bank_code' => strval($bankCode), 
        'account_number' => strval($accountNumber), 
        'description' => strval($description),
        'pin' => "1234"
      );


      $userWallet = $user->wallet->wallet_balance;

      $newWallet = 0;

      $wallet = Wallet::where('user_id', $user->id)->first();
      $wallet->wallet_balance = $newWallet;
      $wallet->save();

      $post = json_encode($post);
      $authorization = "Authorization: Bearer d48b0e8251db0dcbd90ce9a04fc64e6652664fa96d6f954974d8c122efc2d293";

      $ch = curl_init('https://aimtoget.com/api/v1/bank-transfer');
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

      // execute!
      $response = curl_exec($ch);

      // close the connection, release resources used
      curl_close($ch);

      $response = json_decode($response);
      $message = $response->msg;
 
      //Session::flash('message', $message);
      //return Redirect::back();
      if ($message == 'Transfer successful') {
        $userWithdrawal = Withdraw::where('user_id', $user->id)
                          ->where('status', 0)
                          ->first();
        $userWithdrawal->status = 1;
        $userWithdrawal->save();
      }

      return $message;
    }
    
}