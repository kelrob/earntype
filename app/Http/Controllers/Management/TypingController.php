<?php

namespace App\Http\Controllers\Management;

use App\Fund;
use App\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypingController extends Controller
{
    public function addSession()
    {
         return view('management.typing.add');
    }


     public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|max:100',
            'text' => 'required|string',
            'level' => 'required|integer'
        ]);

        $now = Carbon::now();
        $fund  = Fund::whereDate('end_date', '>=', $now)->first();
        
        $price = 500;
        $start_time = now();

        $session = Session::create([
            'type' => $request->type,
            'start_time' => $start_time,
            'price' => $price,
            'text'=> $request->text,
            'level' => $request->level
        ]);

        session()->flash('success', 'Typing session was added successfully');
        return \redirect()->route('management.dashboard');
    }


    
}