<?php

namespace App\Http\Controllers\Management;

use App\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors =Vendor::orderBy('created_at', 'Desc')->paginate(25);
        
        return view('management.vendors.index',[
            'vendors' => $vendors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('management.vendors.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'contact_number' => 'required|numeric|unique:vendors,contact_no',
            'whatsapp' =>'required|string',
            'email' => 'nullable|email|max:100|unique:vendors,email'
        ]);

        $vendor = Vendor::create([
            'name' => $request->name,
            'contact_no' => $request->contact_number,
            'email' => $request->email,
            'whatsapp' => $request->whatsapp,
        ]);

        session()->flash('success', 'New Vendor Added Successfully');
        return \redirect()->route('management.vendors');

    }


}