<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SecurityController extends Controller
{
    public function index()
    {
       return \view('management.password');

    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'new_password' => 'required|string|min:6',
            'old_password' => 'required|string|min:6'
        ]);

        $user = Auth::user();
        $new = $request->new_password;
        $old = $request->old_password;
        $checked = Hash::check($old, $user->password);

        if($checked == true){
            $password = Hash::make($new);
            $user->update([
                'password' => $password
            ]);
            session()->flash('success', 'Password Updated Successfully');
        }else{
            \session()->flash('error', 'Password Mismatch, are you sure you are in th right account? ');
        }

        return \redirect()->route('management.dashboard');
    }
}
