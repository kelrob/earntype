<?php

namespace App\Http\Controllers\Management;

use App\User;
use App\Typing;
use App\Withdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index()
    {
         $withdrawals = Withdraw::orderBy('created_at', 'Desc')->where('status', false)->paginate(25);

        return view('management.index', [
             'pendingWithdraw' => $withdrawals
        ]);
    }

    public function users()
    {
        $users = User::orderBy('username', 'DESC')->paginate(25);
       
        return view('management.users.index', [
            'users' => $users,
           
        ]);
    }

    public function withdrawals()
    {
        $withdrawals =  Withdraw::orderBy('created_at', 'Desc')->where('status', false)->paginate(25);

        return view('management.funding.withdraw', [
            'pendingWithdraw' => $withdrawals
        ]);
    }
    
    public function leader()
    {
        $now = now();
        $participants = Typing::whereDate('created_at', '<', $now)->first();
        return view('management.leaderboard',[
            'participants' => $participants
        ]);
    }
}
