<?php

namespace App\Http\Controllers\Management;

use App\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sessions = Session::orderBy('created_at', 'DESC');
        return view('management.session.index', [
            'sessions' => $sessions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \view('management.session.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|max:100',
            'text' => 'required|string',
            'level' => 'required|integer'
        ]);

        $price = 0;
        $start_time = now();

        $session = Session::create([
            'type' => $request->type,
            'start_time' => $start_time,
            'price' => $price,
            'text'=> $request->text,
            'level' => $request->level
        ]);

        session()>flash('success', 'Typing session was added successfully');
        return \redirect()->route('management.typing.index');
    }


    public function addSession()
    {
       return view('management.typing.add');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
