<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    public function moveReferralBonus()
    {
        $user = Auth::user();
        $wallet = $user->wallet;
        $referral_bonus = $wallet->referral_bonus;
        $wallet_balance = $wallet->wallet_balance;
        $referral = $user->referrals;
        if($referral->count() >= 3){

            $wallet->update([
                'wallet_balance' => $wallet_balance + $referral_bonus,
                'referral_bonus' => 0.00
            ]);
            session()->flash('success', 'Funds was moved successful');
            return \redirect()->route('dashboard');

        }else{
            session()->flash('error', 'Opps! you don\'t have upto 3(three) referrals yet');
            return \redirect()->route('dashboard');
        }
        

    }
}
