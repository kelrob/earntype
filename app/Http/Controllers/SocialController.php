<?php

namespace App\Http\Controllers;

use App\STask;
use App\Social;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{
    
    public function update(Request $request)
    {
        $this->validate($request, [
            'link' => 'required|url'
        ]);
        $user = Auth::user();

        if($request->link != 'https://www.facebook.com/'){
            $social = Social::whereId($user->id)->first();
    
            if($social == true){
                $social->update([
                    'fb_link' => $request->link
                ]);
                \session()->flash('success', 'Facebook link updated successfully');
                return \redirect()->back();
            }else{
                $new_social = Social::create([
                    'user_id' => $user->id,
                    'fb_link' => $request->link,
                ]);
                \session()->flash('success', 'Facebook link added successfully');
                return \redirect()->back();
            }

        }else{
             \session()->flash('error', 'Invalid facebook profile');
                return \redirect()->back();
        }
       

    }

    public function completeTask()
    {
        return view('user.task.complete');
    }

    public function submitTask(Request $request)
    {
        $this->validate($request, [
            'task_name' => 'required|string|max:100',
            'screenshot.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'link'=> 'required|url'
        ]);

        $upload = $request->file('screenshot');

        // get uploaded screenshots
        if ($upload ) {
            foreach($upload as $img) {

                $destinationPath = public_path('/img/screenshots/'); // upload path
                     
                $name= $img->getClientOriginalName();

                $image = time() . '.' . $name;

                $img->move($destinationPath, $image);
              
                $data[] = $image;  
                $screenshots = json_encode($data);
			}

        }

        $user = Auth::user();
        $taskSubmitted = STask::create([
            'user_id' => $user->id,
            'fb-link' => $request->link,
            'task_name' => $request->task_name,
            'done' => true,
            'screenshots' => $screenshots
        ]);

        session()->flash('success', 'Your task submition was successfully, wait a little for confirmation by other users');
        return \redirect()->route('task.index');

    }


    public function taskUndone($id)
    {
       $task = STask::whereId($id)->first();

       $task->upate([
           'marks_undone' => $task->marks_undone + 1
       ]);

       if($task->marks_done >= 5 ){
           
           $task->update([
               'done' => true
           ]);
       }
       
       $name = $task->user->username;
       $date = Carbon::parse($task->created_at)->format('d-m-Y');

       session()->flash('success', "$name task for $date was marked undone");
       return \redirect()->route();
    }

    public function taskDone($id)
    {
        $task = STask::whereId($id)->first();
        $task->update([
           'marks_done' => $task->marks_done + 1
       ]);
        
       if($task->marks_done >= 5 ){

           $task->update([
               'done' => true
           ]);
       }
       $name = $task->user->username;
       $date = Carbon::parse($task->created_at)->format('d-m-Y');

       session()->flash('success', "$name task for $date was marked done");
       return \redirect()->back();
    }

    
}
