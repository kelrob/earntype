<?php

namespace App\Http\Controllers;

use App\Typing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $wallet = $user->wallet;
        $typing = Typing::orderBy('position', 'Desc')->take(20)->get();

        $time = Carbon::now();
        $morning = Carbon::create($time->year, $time->month, $time->day, 8, 0, 0); //set time to 08:00
        $morning = Carbon::create($time->year, $time->month, $time->day, 12, 0, 0); //set time to 12:00
        $evening = Carbon::create($time->year, $time->month, $time->day, 18, 0, 0); //set time to 18:00

        // $typing = Typing::where('created_at', '<', $time)->take(10);
        return view('user.index', [
            'wallet' => $wallet,
            'typings' => $typing
        ]);
    }
}
