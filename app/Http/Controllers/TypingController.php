<?php

namespace App\Http\Controllers;

use App\Typing;
use App\Session;
use App\SessionRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TypingController extends Controller
{
    public function start()
    {
       $user = Auth::user();
       $typed = Typing::where('user_id', $user->id)->first();
       if($typed == true){
           session()->flash('error', 'You are already part of this session');
           return \redirect()->back();
       }

      $session = Session::inRandomOrder()->limit(1)->first();
      $userPlayedCount = SessionRecord::where('user_id', $user->id)->count();
      $userSession = SessionRecord::where('user_id', $user->id)->first();

      return view('user.task.typing', compact('session', 'userPlayedCount', 'userSession'));
    }

    public function submit(Request $request, $id)
    {
        $this->validate($request,[
            'text' => 'required|string',
        ]);

        $date = now();
        $eartype = Session::whereDate('end_time' , '>' , $date)->first();

        //Get time in seconds
        $str_time = $request->time;
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;

        //split typed text into an array of words
        $text = $request->text;
        $words = explode(" ", $text);

        $main = '';//correct text
        $mainWords = explode(" ", $main);//correct text splitted in words

        $error = 0;//error in words
        $correct = 0;//correct words
        
        //get the correct and incorrect words
        for($i = 0; $i < count($words); $i++){
            if($words[$i] != $mainWords[$i]){
                $error = $error + 1;
            }else{
                $correct = $correct + 1;
            }
        }
        $totalWords = count($mainWords);
        $typedWords = count($words);
        $skipedWords = $totalWords - $typedWords;

        $level = '';
        $position = 0;
        

        $user = Auth::user();

        $typing  = Typing::create([
            'user_id' => $user->id,
            'username'=> $user->username,
            'sec' => $time_seconds,
            'errors' => $error,
            'correct' => $correct,
            'words_typed' => $typedWords,
            'skipped_words' => $skipedWords,
            'session' => $eartype->sessioon,
            'position' => $position,
            'level' => $level,
        ]);

       
        session()->flash('success', 'Thanks for joining this typing session your result will be out soon');
        return redirect()->route('dashboard');
       
    }
}
