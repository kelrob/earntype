<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Banking extends Component
{
    public $bank, $account_name, $account_number;

    public function mount()
    {
        
    }
    
    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'bank' => 'required|string|max:100',
            'account_number' => 'required|numeric|unique:bankings',
            'account_name' => 'required|string|max:50'
        ]);

    }

    public function bankDetails()
    {
        $this->validate([
            'bank' => 'required|string|max:100',
            'account_number' => 'required|numeric|unique:bankings',
            'account_name' => 'required|string|max:50'
        ]);

        return redirect()->route('earning.index');
    }
    public function render()
    {
        return view('livewire.banking');
    }
}
