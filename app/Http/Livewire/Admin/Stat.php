<?php

namespace App\Http\Livewire\Admin;

use App\User;
use App\Coupon;
use App\Vendor;
use App\Withdraw;
use Livewire\Component;

class Stat extends Component
{
    public $users, $coupons, $vendors, $pendingWithdraw;

    public function mount()
    {
       $users = User::all();
       $this->users = $users;
       $coupons = Coupon::all();
       $this->coupons = $coupons;

       $vendors = Vendor::all();
       $this->vendors = $vendors;

       $pending = Withdraw::where('status', false)->get();
       $this->pendingWithdraw = $pending;

    }
    
    public function render()
    {
        return view('livewire.admin.stat');
    }
}
