<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Referrals extends Component
{
    public $referrals, $user, $referral_bonus;

    public function mount()
    {
        $this->user = Auth::user();
        $this->referrals = $this->user->referrals;
        $this->referral_bonus = number_format($this->user->wallet->referral_bonus,2);
    }

    public function render()
    {
        return view('livewire.referrals');
    }
}
