<?php

namespace App\Http\Middleware;

use Closure;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(auth()->user() && auth()->user()->is_admin == 1){
            return $next($request);
        }
   
        \session()->flash('error',"You don't have admin access.");
        return abort(403);
        // return $next($request);
    }
}
