<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class STask extends Model
{
    protected $fillable = [
        'user_id',
        'fb-link',
        'task_name',
        'done',
        'marks_done',
        'marks_undone',
        'screenshots',
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
