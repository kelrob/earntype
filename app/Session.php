<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable =[
        'type',
        'end_time',
        'price',
        'text',
        'level',
    ];
}
