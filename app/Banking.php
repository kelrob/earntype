<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banking extends Model
{
    use SoftDeletes;
    
    protected $fillable =[
        'user_id',
        'bank_name',
        'bank_code',
        'account_name',
        'account_number',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
}
