<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'code',
        'given_to',
        'status',
        'given',
    ];
    

    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }


}
