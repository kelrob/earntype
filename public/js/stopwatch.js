function Stopwatch(elem) {
    var time = 0;
    var interval;
    var offset;
    function update() {
        time += delta();
        var formattedTime = timeFomatter(time)
        elem.textContent = formattedTime;
    }
    function delta() {
        var now = Date.now();
        var timePassed = now - offset;
        offset = now;
        return timePassed;
    }
    function timeFomatter(timeInMilliSeconds) {
        var time = new Date(timeInMilliSeconds);
        var hour = 0;
        var min = time.getMinutes().toString();
        var sec = time.getSeconds().toString();
        var milSec = time.getMilliseconds().toString();

        if(min == 60){
            hour = hour+1;
        }
        if(hour.toString().length < 2){
            hour = '0'+hour;
        }
        if(min.length < 2){
            min = '0'+min;
        }

        if (sec.length < 2) {
            sec = '0' + sec;
        }

        while (milSec.length < 3) {
            milSec = '0' + milSec;
        }
        return hour + ':' + min + ':' + sec;
    }

    this.isOn = false;
    this.start = function () {
        if (!this.isOn) {
            interval = setInterval(update, 10);
            offset = Date.now();
            this.isOn = true;
        }
    };
    this.stop = function () {
        if (this.isOn) {
            clearInterval(interval);
            interval = null;
            this.isOn = false;
        }
    };

    this.reset = function () {
        time = 0;
    };

}

