<div class="row">
    <div class="col-sm-12 mt-4 mt-lg-0">
        <div class="bg-primary text-white px-4 py-4 card">
            <div class="row">
                <div class="col-sm-6 pl-lg-5">
                    <h3>{{$referrals->count()}}</h3>
                    <p class="mb-0">{{ $referrals->count() > 1 ? 'Referrals' : 'Referral'}}</p>
                </div>
                <div class="col-sm-6 climate-info-border mt-lg-0 mt-2">
                    <h3> <small>&#x20a6;</small>{{$referral_bonus}}</h3>
                    <p class="mb-0">Referral Bonus</p>
                </div>
            </div>
        </div>
    </div>
</div>
