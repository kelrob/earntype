<div class="row mt-5">
    <div class="col-md-8 offset-md-2">
        <div class="text-center">
            Enter Your Banking Information
        </div>
        <form wire:submit.prevent="bankDetails" method="post">

            <div class="form-group">
                <label>Bank</label>
                <select wire:model="bank" class="js-example-basic-single w-100">
                    <option value="AL">UBA</option>
                    <option value="WY">First Bank Plc</option>
                    <option value="AM">FCMB</option>
                    <option value="CA">PAGA</option>
                    <option value="RU">Union Bank</option>
                </select>
            </div>

            <div class="form-group">
                <label for="exampleInputUsername1">Account Name</label>
                <input wire:model="account_name" type="text" class="form-control" id="exampleInputUsername1" placeholder="Account Name">
                @error('account_name')
                    <span class="text-danger"> {{$message}} </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="exampleInputUsername1">Account Number</label>
                <input wire:model="account_number" type="text" class="form-control" id="exampleInputUsername1" placeholder="Account Number">
                 @error('account_number')
                    <span class="text-danger"> {{$message}} </span>
                @enderror
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-outline-inverse-primary form-control font-weight-700">Continue</button>
            </div>
        </form>
    </div>
</div>

