
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    {{config('app.name')}} - @yield('title')
  </title>
  <!-- Favicon -->
  {{-- <link href="../assets/img/brand/favicon.png" rel="icon" type="image/png"> --}}
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="/dash/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
  <link href="/dash/js/plugins/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="/dash/css/argon-dashboard.css?v=1.1.2" rel="stylesheet" />
</head>

<body class="bg-default">
  <div class="main-content">
    
    <div class="header bg-gradient-primary py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white"><a href="/" class="text-white">{{config('app.name')}}</a></h1>
                <p class="text-lead text-light">
                  @if (\Route::is('register')) Register and create a new account Or <a href="{{route('login')}}"style="color: skyblue;">login</a> to a existing account 
                  @elseif(\Route::is('login'))
                   Login to your account below. Or click <a href="{{route('register')}}"style="color: skyblue;">register</a>  Create a new account 
                   @else
                   @yield('first')
                   @endif 
                   
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>

    @yield('content')
    


    <footer class="py-5">
      <div class="container">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; {{now()->format('Y')}} <a href="/" class="font-weight-bold ml-1" target="_blank">{{config('app.name')}} All Rights Reserved</a>
            </div>
          </div>
         
        </div>
      </div>
    </footer>
  </div>

 <!--   Core   -->
  <script src="/dash/js/plugins/jquery/dist/jquery.min.js"></script>
  <script src="/dash/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!--   Optional JS   -->
  <!--   Argon JS   -->
  <script src="/dash/js/argon-dashboard.min.js?v=1.1.2"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
</body>

</html>