<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{config('app.name')}} - @yield('title')</title>
    <!-- base:css -->
    <link rel="stylesheet" href="/dash/user/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/dash/user/vendors/base/vendor.bundle.base.css">
    
    <link rel="stylesheet" href="/dash/user/css/style.css">
    <link rel="stylesheet" href="/dash/user/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="/dash/user/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
    <script src="https://kit.fontawesome.com/f607648d58.js" crossorigin="anonymous"></script>
    <!-- endinject -->
    {{-- <link rel="shortcut icon" href="images/favicon.png" /> --}}
    @livewireStyles()
    @livewireScripts()
    <script src="/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="/js/stopwatch.js"></script>
  </head>
  <style>
      .logo{
          font-family: Georgia, 'Times New Roman', Times, serif;
          font-weight: 800;
          padding: 0 20px;
      }
  </style>
  <body>
    <div class="container-scroller">
		  <div class="horizontal-menu">
            <nav class="navbar top-navbar col-lg-12 col-12 p-0">
                <div class="container-fluid">
                <div class="navbar-menu-wrapper d-flex align-items-center justify-content-between">
                   
                    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                        <a class="navbar-brand brand-logo" href="index.html">
                            
                            <h2 class="logo">EarnType</h2>
                            
                        </a>
                        <a class="navbar-brand brand-logo-mini" href="index.html">
                            
                           <h4 class="logo" style="margin-top: 0.3rem;">EarnType</h4>
                        </a>
                    </div>
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                                <span class="nav-profile-name">{{Auth::user()->username}}</span>
                                <span class="online-status"></span>
                                <img src="/img/avatar.jpg" alt="profile"/>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                                <a class="dropdown-item" href="{{route('settings')}}">
                                    <i class="mdi mdi-settings text-primary"></i>
                                    Settings
                                </a>
                                <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="mdi mdi-logout text-primary"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                    <span class="mdi mdi-menu"></span>
                    </button>
                </div>
                </div>
            </nav>
            <nav class="bottom-navbar">
                <div class="container">
                    <ul class="nav page-navigation">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('dashboard')}}">
                        <i class="mdi mdi-file-document-box menu-icon"></i>
                        <span class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('board')}}" class="nav-link">
                            <i class="mdi mdi-grid menu-icon"></i>
                            <span class="menu-title">Leaderboard</span>
                            <i class="menu-arrow"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('task.index')}}" class="nav-link">
                            <i class="mdi mdi-finance menu-icon"></i>
                            <span class="menu-title">Tasks</span>
                            <i class="menu-arrow"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('earning.index')}}" class="nav-link">
                            <i class="mdi mdi-chart-areaspline menu-icon"></i>
                            <span class="menu-title">Withdraw</span>
                            <i class="menu-arrow"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
                            <i class="mdi mdi-logout menu-icon"></i>
                            <span class="menu-title">Logout</span>
                            <i class="menu-arrow"></i>
                        </a>
                    </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="container-fluid page-body-wrapper">
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
						<div class="col-sm-6 mb-4 mb-xl-0">
							<div class="d-lg-flex align-items-center">
								<div>
									<h2 class="text-dark font-weight-bold mb-2"> <small>#</small> 50.00</h2>
									<h5 class="font-weight-normal mb-2">Today Earning</h5>
								</div>
								
							</div>
                        </div>
                        
						<div class="col-sm-6">
							<div class="d-flex align-items-center justify-content-md-end">
                                <div class="pr-1 mb-3 mb-xl-0">
                                    <a href="{{route('typing.start')}}" class="btn btn-primary btn-icon-text">
											Start Typing
											<i class="mdi mdi-keyboard btn-icon-append"></i>                          
                                        </a>
								</div>
								
							</div>
						</div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 flex-column d-flex stretch-card">
                            @include('inc.message')
                            <div class="mt-3">
                                @yield('content')
                            </div>  
                        </div>
                    <div class="col-sm-4 flex-column d-flex stretch-card">
                        <div class="row flex-grow mt-3">
                            <div class="col-sm-12 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <h3 class="font-weight-bold text-dark">Referral Section</h3>
                                                <p class="text-dark">Share link below to get referral bonus</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 mt-4">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="link" value="{{config('app.url').'/register?ref='.auth()->user()->referral_token}}" placeholder="Recipient's username" aria-label="Recipient's username">
                                                    <div class="input-group-append">
                                                        <button onclick="event.preventDefault(); copyReferralLink();" class="btn btn-sm btn-primary" type="button">Copy Link</button>
                                                    </div>
                                                </div>
                                            </div> 
                                            </div>
                                            
                                        </div>

                                            @livewire('referrals')

                                        <div class="mt-4 flex-row text-center">
                                            <a href="{{route('moveTowallet')}}" class="btn btn-outline-inverse-primary">Move Bonus to wallet</a>
                                        </div>
                                        <div class="row pt-3 mt-md-1">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                    <form action="{{route('logout')}}" id="logout-form" style="display: none;" method="post">
                        @csrf
                    </form>
                </div>

                <footer class="footer">
					<div class="footer-wrap">
						<div class="w-100 clearfix">
                            <span class="d-block text-center text-sm-left d-sm-inline-block">Copyright © {{now()->format('Y')}} <a href="/" >{{config('app.name')}}</a>. All rights reserved.</span>
							{{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart-outline"></i></span> --}}
						</div>
					</div>
				</footer>
            </div>
        </div>
    </div>


    	<!-- container-scroller -->
    <!-- base:js -->
    <script src="/dash/user/vendors/base/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="/dash/user/js/template.js"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <!-- End plugin js for this page -->
    <script src="/dash/user/vendors/chart.js/Chart.min.js"></script>
    <script src="/dash/user/vendors/progressbar.js/progressbar.min.js"></script>
    <script src="/dash/user/vendors/chartjs-plugin-datalabels/chartjs-plugin-datalabels.js"></script>
    <script src="/dash/user/vendors/justgage/raphael-2.1.4.min.js"></script>
    <script src="/dash/user/vendors/justgage/justgage.js"></script>
    <script src="/dash/user/vendors/typeahead.js/typeahead.bundle.min.js"></script>
    <script src="/dash/user/vendors/select2/select2.min.js"></script>
    <script src="/dash/user/js/file-upload.js"></script>
    <script src="/dash/user/js/typeahead.js"></script>
    <script src="/dash/user/js/select2.js"></script>
    <!-- Custom js for this page-->
    <script src="/dash/user/js/dashboard.js"></script>
    <!-- End custom js for this page-->
    <script>
        function copyReferralLink() {
            var copyText = document.getElementById("link");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
        }
    </script>
    @yield('js')
  </body>
</html>
