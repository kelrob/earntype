	<!DOCTYPE HTML>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <!-- Facebook and Twitter integration -->
          <meta property="og:title" content=""/>
          <meta property="og:image" content=""/>
          <meta property="og:url" content=""/>
          <meta property="og:site_name" content=""/>
          <meta property="og:description" content=""/>
          <meta name="twitter:title" content="" />
          <meta name="twitter:image" content="" />
          <meta name="twitter:url" content="" />
          <meta name="twitter:card" content="" />

        <title>{{config('app.name')}} - @yield('title')</title>


        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400" rel="stylesheet">
        <script src="https://kit.fontawesome.com/f607648d58.js" crossorigin="anonymous"></script>
	
        <!-- Animate.css -->
        <link rel="stylesheet" href="main/css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="{{asset('main/css/icomoon.css')}}">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="main/css/bootstrap.css">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="main/css/magnific-popup.css">

        <!-- Owl Carousel -->
        <link rel="stylesheet" href="main/css/owl.carousel.min.css">
        <link rel="stylesheet" href="main/css/owl.theme.default.min.css">

        <!-- Theme style  -->
        <link rel="stylesheet" href="main/css/style.css">

        <!-- Modernizr JS -->
        <script src="main/js/modernizr-2.6.2.min.js"></script>
         <script src="/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="/js/stopwatch.js"></script>

        @yield('added')
        <style>
            #links{
                display: flex; 
                justify-content:center; 
                align-items: center;
                margin-bottom: 10px; 
                overflow-x: auto;
                
            }
            
        </style>

    </head>
    
	<body>
        <div class="colorlib-loader"></div>
    
        <div id="page">
            <nav class="colorlib-nav" role="navigation">
                <div class="top-menu @if(\Request::is('/')) @else bg-primary @endif">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2">
                                <div id="colorlib-logo"><a href="/">EarnType</a></div>
                            </div>
                            <div class="col-md-10 text-right menu-1">
                                <ul>
                                    <li class="active"><a @if(\Request::is('/')) @else href="/#home" @endif href="#home">Home</a></li>
                                    <li><a href="{{route('how')}}">How It Works</a></li>
                                    <li><a href="{{route('faq')}}" >Faqs</a></li>
                                    <li><a href="/vendors">Coupon Vendors</a></li>
                                    @guest
                                        <li class="has-dropdown">
                                            <a href="#" onclick="event.preventDefault();">Account</a>
                                            <ul class="dropdown">
                                                <li><a href="{{route('login')}}">Login</a></li>
                                                <li><a href="{{route('register')}}">Register</a></li>
                                            </ul>
                                        </li>
                                    @else
                                        @if (Auth::user()->is_admin == true)
                                            
                                        @else
                                            <li class="has-dropdown">
                                                <a href="#" onclick="event.preventDefault();">{{Auth::user()->username}}</a>
                                                <ul class="dropdown">
                                                    <li><a href="{{route('dashboard')}}">Dasboard</a></li>
                                                    <li><a href="{{route('register')}}">Settings</a></li>
                                                    <li><a onclick="event.preventDefault(); document.getElementById('logout-form').submit()" href="{{route('logout')}}">Logout</a></li>
                                                </ul>
                                                <form action="{{route('logout')}}" style="display: none;" id="logout-form" method="post">
                                                    @csrf
                                                </form>
                                            </li>
                                        @endif
                                    @endguest
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            @yield('content')

            <footer id="colorlib-footer">
			
				<div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="mt--4" id="links">
                                <a href="/">Home</a>  <a href="{{route('how')}}">How It Works</a>
                                 <a href="{{route('faq')}}">Faqs</a>  <a href="{{route('terms')}}">Terms & conditions</a>
                                 <a href="{{route('policy')}}">Policy</a>
                            </div>
                            
                            <p>
                                Contact: contact us via email: earntypeng@gmail.com , <br> Contact no: +2349017138613 , <br> Facebook : <a href="https://web.facebook.com/earntype"> https://web.facebook.com/earntype</a>
                            </p>
                            <p>
                                Copyright &copy; {{config('app.name')}} 
                                <script>document.write(new Date().getFullYear());</script>
                                    All rights reserved | Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <br> 
                            </p>
                            
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <div class="gototop js-top">
            <a href="#" class="js-gotop">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
		
    
    <!-- jQuery -->
	<script src="main/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="main/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="main/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="main/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="main/js/jquery.stellar.min.js"></script>
	<!-- YTPlayer -->
	<script src="main/js/jquery.mb.YTPlayer.min.js"></script>
	<!-- Owl carousel -->
	<script src="main/js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="main/js/jquery.magnific-popup.min.js"></script>
	<script src="main/js/magnific-popup-options.js"></script>
	<!-- Counters -->
	<script src="main/js/jquery.countTo.js"></script>
	<!-- Main -->
    <script src="main/js/main.js"></script>
    
    @yield('addedB')

	</body>
</html>