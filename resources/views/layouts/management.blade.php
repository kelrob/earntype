<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    {{config('app.name')}} - @yield('title')
  </title>
  <!-- Favicon -->
  {{-- <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png"> --}}
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="/dash/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
  <link href="/dash/js/plugins/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="/dash/css/argon-dashboard.css?v=1.1.2" rel="stylesheet" />
  @livewireStyles()
  @livewireScripts()
</head>

<body>
  @include('inc.sidebar')
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="/">@yield('header')</a>
        
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{asset('img/avatar.jpg')}}">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{ucfirst(auth()->user()->username)}}</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              
              <a href="{{route('management.vendors.add')}}" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Add Vendors</span>
              </a>
              <a href="{{route('management.coupon.create')}}" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Create Coupon</span>
              </a>
              
              <div class="dropdown-divider"></div>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <div class="mt-3">
      @include('inc.adminmessage')
    </div>
    <!-- End Navbar -->
    @livewire('admin.stat')
    
  
    
    @yield('content')
</div>

    <!--   Core   -->
  <script src="/dash/js/plugins/jquery/dist/jquery.min.js"></script>
  <script src="/dash/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!--   Optional JS   -->
  <script src="/dash/js/plugins/chart.js/dist/Chart.min.js"></script>
  <script src="/dash/js/plugins/chart.js/dist/Chart.extension.js"></script>
  <!--   Argon JS   -->
  <script src="/dash/js/argon-dashboard.min.js?v=1.1.2"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
</body>
</html>