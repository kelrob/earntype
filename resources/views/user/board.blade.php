@extends('layouts.dashboard')
@section('title', 'LeaderBoard')
@section('content')
    <div class="card">
        <div class="card-body">
          <h4 class="card-title">LeaderBoard</h4>
          <p class="card-description">
            Total Participants for Morning Session
          </p>
          @if ($typings->count() > 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                    <thead>
                        <tr>
                        <th>User</th>
                        <th>Position</th>
                        <th>Errors</th>
                        <th>Words Typed</th>
                        <th>Amount Earned</th>
                        <th>Time (sec)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($typings as $type)
                            <tr>
                                <td>{{$type->username}}</td>
                                <td class="text-success"> {{$type->position}} <i class="mdi mdi-arrow-up"></i></td>
                                <td>{{$type->errors}}</td>
                                <td>{{$type->words_typed}}</td>
                                <td>{{'oi'}}</td>
                                <td>
                                    {{$type->sec}} secs
                                </td>
                            </tr>
                            
                        @endforeach
                        
                    </tbody>
                    </table>
                </div>
                    
                @endif
        </div>
      </div>
@endsection