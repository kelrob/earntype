@extends('layouts.dashboard')
@section('title', 'Tasks')
@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tasks</h4>
            <div class="mt-5">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="text-center">Facebook Profile</h3>
                        <form action="{{route('update.fb')}}" method="post">
                            @csrf
                        <div class="input-group mt-5">
                                <input type="text" name="link" class="form-control" value="{{ $link }}" placeholder="Enter a link to your facebook account" aria-label="Recipient's username">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-sm btn-primary" type="button">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <h3 class="text-center">Status</h3>
                        <div class="row mt-5">
                            <div class="col-6">
                                <div class="text-center">
                                    <h4>{{$myTasks->sum('marks_done')}}</h4>
                                    <p class="text-success">Marks Undone</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="text-center">
                                    <h4>{{$myTasks->sum('marks_undone')}}</h4>
                                    <p class="text-danger">Marks Done</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div style="display: flex; justify-content: center; align-items: center; margin: 20px 0 0;">
               
                    <a href="https://web.facebook.com/earntype" target="_blank" class="mr-4">
                        <span>
                            <i class="fab fa-facebook"></i>
                        </span>
                        Join us on facebook
                    </a>
                
                
                    <a href="#">
                        <span>
                            <i class="fab fa-telegram-plane"></i>
                        </span>
                        Join us on telegram
                    </a>
               
            </div>

            <div class="table-responsive mt-5">

                @if ($tasks && $tasks->count() > 0)
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>
                                user
                            </th>
                            <th>
                                task name
                            </th>
                            <th>
                                date submitted
                            </th>
                        
                            <th>
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($tasks as $task)
                                <tr>
                                    <td>
                                        <strong>
                                        {{$task->user->username}}
                                        </strong>
                                    </td>
                                    <td>
                                        <p>
                                            {{$task->task_name}}
                                        </p>
                                    </td>
                                    <td>
                                        <p>
                                            {{\Carbon\Carbon::parse($task->created_at)->diffForHumans()}}
                                        </p>
                                    </td>
                                    
                                    <td>
                                        <a href="{{route('task.markdone', $task->id)}}" class="text-danger mr-3 text-bold"> done</a>
                                        <a href="{{route('task.markundone', $task->id)}}" class="text-danger text-bold"> not done</a>
                                    </td>
                                </tr>
                            @endforeach
                        
                        </tbody>
                    </table>
                @endif
               
            </div>


            <div class="mt-5">
                <h4 class="text-center text-success mb-2">EarnType - Winapay #2,000 reward task</h4>
                <p class="text-center">Please do share a screenshot of your WatsApp status </p>

                <div class="mt-3">
                    <div class="py-2 border-bottom border-dark">
                        <a href="{{route('task.complete')}}" class="btn btn-primary">Complete Today's Task</a>
                    </div>
                </div>
            </div>

            <div class="mt-4 py-3 text-center bg-warning">
                <p>Please note that the task is cumpolsary for you as a member to withdraw your daily earnings</p>
            </div>
        </div>
    </div>
@endsection
