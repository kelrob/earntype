@extends('layouts.dashboard')
@section('title', 'Complete a Tasks')
@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Complete A Tasks</h4>
            <div class="mt-5 row">
                <div class="col-md-8 offset-md-2">
                   <p class="text-center">Submit Todays task</p>

                   <div class="mt-4">
                        <form action="{{route('task.submit')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="taskName">Task Name</label>
                                <input type="text" name="task_name" class="form-control" id="taskName" placeholder="Enter the Task name">
                                @error('task_name')
                                    <p>
                                        <span class="text-danger">{{$message}}</span>
                                    </p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="taskName">Facebook Url</label>
                                <input type="text" name="link" class="form-control" id="taskName" placeholder="Enter Facebook link">
                                @error('link')
                                    <p>
                                        <span class="text-danger">{{$message}}</span>
                                    </p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Upload Screenshots</label>
                                <input type="file" name="screenshot[]" class="file-upload-default" multiple>
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>

                                @error('screenshot')
                                    <p>
                                        <span class="text-danger">{{$message}}</span>
                                    </p>
                                @enderror
                            </div>

                            <div class="mt-4 pb-5">
                                <button type="submit" class="btn btn-block btn-primary">Submit task</button>
                            </div>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection
