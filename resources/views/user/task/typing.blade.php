@extends('layouts.dashboard')
@section('title', 'Start Typing')
@section('added')
    
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Typing</h4>
            @if($userPlayedCount == 0)
              <div class="mt-5">
                <div class="mt-5 text-center">
                    <h4 class="text-warning" id="text-heading">Please type the text below</h4>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                    <b id="timer"><span id="minutes"></span>:<span id="seconds"></span></b>
                    <p id="text" style="font-size: 1.2rem; letter-spacing: 1px;" class=" p-3 bg-success text-white">
                      {{ $session->text }}
                    </p>
                </div>
              
                <form action="#" autocomplete="off" id="demo-game-form" style="">
                            <div id="game">
                              
                                
                                <div class="row form-group" >
                                    <div class="col-lg-12">
                                      {{-- <p class="text-center"><b>{{ $game->string }}</b></p> --}}
                                    </div>
                                    <div class="col-md-12">
                                      <div id="error-message" style="display: none">
                                        <div class="alert alert-danger">Texts does not match</div>
                                      </div>
                                      <div id="success-message" style="display: none">
                                        <div class="alert alert-success">Submitted to DB</div>
                                      </div>
                                        <div>
                                            Start Typing
                                        </div>
                                        <textarea name="message" id="textarea" onkeydown="hideError()" cols="30" rows="1" class="form-control"
                                          oncopy="return false" onpaste="return false" style="resize: none;"></textarea>
                                    </div>
                                  <p style="display: none;" id="token">{{ time() }}</p>
                                    <div class="col-md-12" style="margin-top: 2%;" align="center">
                                      <a onclick="processType()" class="btn btn-success" id="btn-submit" style="width: 50%;">Submit</a>
                                    </div>
                                </div>
                                
                            </div>

                            {{-- <div class="form-group">
                                <input  type="submit" value="" class="btn btn-primary">
                            </div> --}}
                        </form>
                
                 <div class="row" id="result-form" style="display: none;">
                    <div class="col-lg-12" style="background-color: #fff; padding: 2%; border-radius: 4px;">
                      <h3 class="text-center">Wow <span id="d_fullname"></span>! You Spent</h3>
                      <h2 class="text-center" style="color: #337AB7;"><span id="d_time"></span></h2>
                      {{-- <p class="text-center"><a href="" data-toggle="modal" data-target="#earning-modal" class="btn btn-primary">See your earnings</a></p> --}}
                    </div>
                  </div>
            </div>
            @else
              <div class="row" id="result-form">
                    <div class="col-lg-12" style="background-color: #fff; padding: 2%; border-radius: 4px;">
                    <h3 class="text-center">Wow <span id="d_fullname">{{ Auth::user()->username }}</span>! You Spent</h3>
                    <h2 class="text-center" style="color: #337AB7;"><span id="d_time">00:{{ substr($userSession->time_spent , 0, -3) }}</span></h2>
                      {{-- <p class="text-center"><a href="" data-toggle="modal" data-target="#earning-modal" class="btn btn-primary">See your earnings</a></p> --}}
                    </div>
                </div>
            @endif
        </div>

         <script>
          

            const storeLocal = () => {
              let fullname = {{Auth::user()->username}};

              if (fullname === '') {
                alert('All Fields are required');
              } else {
                // Store to local storage
                localStorage.setItem('fullname', fullname);

                $('#details-form').hide();
                $('#demo-game-form').show();
                startTimer();
              }
            }
          </script>

          <script>
            var sec = 0;
            function pad ( val ) { return val > 9 ? val : "0" + val; }
            
              setInterval( function() {
                  $("#seconds").html(pad(++sec%60));
                  $("#minutes").html(pad(parseInt(sec/60,10)));
              }, 1000)
            
            const hideError = () => {
              $('#error-message').hide();
            }
            const processType = () => {
              let actualText = $('#text').html();
              let textEntered = $('#textarea').val();
              let timeSpent = $('#timer').text();
              let fullname = `{{Auth::user()->username}}`;
              if (textEntered === '') {
                textEntered = 'null'
              }
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: { 
                  "actualText": actualText,
                  "textEntered": textEntered,
                  "token_t": {{ time() }},
                  "t_s": timeSpent,
                },
                type: "POST",
                url: `record-session-live`,
                beforeSend: function () {
                  
                },
                success: function (data) {
                  if (data == 'wrong') {
                    $('#error-message').show();
                  }

                  if (data == 'correct') {
                    localStorage.setItem('timespent', timeSpent);
                    localStorage.setItem('fullname', fullname);
                    $('#details-form').hide();
                    $('#demo-game-form').hide();
                    $('#timer').hide();
                    $('#text').hide();
                    $('#text-heading').hide();
                    $('#d_fullname').text(localStorage.getItem("fullname"));
                    $('#d_time').text(localStorage.getItem("timespent"));
                    $('#result-form').show();


                  }
                  
                }
              });
            }
          </script> 

    </div>
    
    

@endsection

@section('js')
<script src="js/stopwatch.js"></script>
    <script>
        $(document).ready(function(){
            var timer = document.getElementById('timer');
            var watch = new Stopwatch(timer);
            $("#text, #typing").bind("cut copy paste", function(e) {
                e.preventDefault();
                console.log('Operation Prevented');
            })

            $("#typing").one("input", function () {
                watch.start();
            });

            $("#finish").click(function(e){
                e.preventDefault();
                var stop = watch.stop();
                var timing = document.getElementById('timer').innerHTML;
                document.getElementById('time').value = timing;
                var form = document.getElementById('submitTyping').submit();
            });
           
        })
    </script>
    
@endsection
