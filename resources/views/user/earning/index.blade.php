@extends('layouts.dashboard')
@section('title', 'Earning')
@php
    $bank = App\Banking::where('user_id', auth()->user()->id)->first();
@endphp
@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Withdraw</h4>
            <div class="my-3 p-4 bg-warning">
              <p>Please note that you wil be charged &#8358;100 per request</p>
            </div>
            @if ($bank == true)
               
                <div class="mt-5 text-md-left text-center">
                    <a @if (auth()->user()->wallet->wallet_balance >= 500)
                        href="{{route('request-withdraw')}}"
                    @else
                        href = "#" onclick="alert('Minimum withdrawal request is &#8358;500')"
                    @endif class="btn btn-primary btn-lg">
                        <i class="mdi mdi-cash"></i>                      
                        Request Withdraw
                    </a>
                </div>

                <div class="table-responsive mt-5">
                    @if ($withdraws->count() > 0)
                         <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>
                                Amount
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                Account To
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                              
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                             @foreach ($withdraws as $withdraw)
                                <tr>
                                    <td>
                                        <strong>
                                            &#x20a6;{{$withdraw->amount}}
                                        </strong>
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($withdraw->created_at)->diffForHumans()}}
                                    </td>
                                    <td>
                                        {{$withdraw->account_number}}
                                    </td>
                                    <td>
                                        @if ($withdraw->status == true)
                                        <label class="badge badge-success">success</label>
                                        @else
                                        <label class="badge badge-warning">pending</label> 
                                        @endif
                                    </td>
                                    <td>
                                    
                                    </td>
                                </tr>
                            @endforeach
                        
                        </tbody>
                    </table>
                       
                    @else
                        
                    @endif
                   
                </div>
            @else
               <div class="row mt-5">
                    <div class="col-md-8 offset-md-2">
                        <div class="text-center">
                            Enter Your Banking Information Before Requesting Withdraw
                        </div>
                        <form action="{{ route('storeBankDetails') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Bank</label>
                               <select name="bankName" class="form-control">
                                    <option value="ECOBANK,000010">ECOBANK</option>
                                    <option value="FCMB,000003">FCMB</option>
                                    <option value="FIDELITY,000007">FIDELITY</option>
                                    <option value="FIRST BANK,000016">FIRST BANK</option>
                                    <option value="GT BANK,000013">GT BANK</option>
                                    <option value="HERITAGE BANK,000020">HERITAGE BANK</option>
                                    <option value="KEYSTONE BANK,000002">KEYSTONE BANK</option>
                                    <option value="Abbey Mortgage Bank,070010"> Abbey Mortgage Bank</option>
                                    <option value="Accion Mfb,090134"> Accion Mfb</option>
                                    <option value="Ag Mortgage Bank Plc,100028"> Ag Mortgage Bank Plc</option>
                                    <option value="Al-barkah Mfb,090133"> Al-barkah Mfb</option>
                                    <option value="Allworkers Mfb,090131"> Allworkers Mfb</option>
                                  
                                </select>
                                 @error('bankName')
                                    <span class="text-danger"> {{$message}} </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Account Name</label>
                                <input name="accountName" type="text" class="form-control"  placeholder="Account Name">
                                @error('accountName')
                                    <span class="text-danger"> {{$message}} </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Account Number</label>
                                <input name="accountNumber" type="text" class="form-control"  placeholder="Account Number">
                                @error('accountNumber')
                                    <span class="text-danger"> {{$message}} </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-inverse-primary form-control font-weight-700">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>

            @endif
            
        </div>
    </div>
@endsection