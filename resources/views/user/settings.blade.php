@extends('layouts.dashboard')
@section('title', 'Settings')
@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Settings</h4>

            <div class="row mt-5">
            <div class="col-md-8 offset-md-2">
                
            <form method="post" action="{{route('update-settings')}}">
                @csrf
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" id="exampleInputUsername1" placeholder="Username" value="{{Auth::user()->username}}">
                        @error('username')
                            <span class="text-danger"> {{$message}} </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" value="{{Auth::user()->email}}" placeholder="Email">
                        @error('email')
                            <span class="text-danger"> {{$message}} </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-inverse-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection