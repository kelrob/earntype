@extends('layouts.dashboard')
@section('title', auth()->user()->username . ' Dashboard')
@section('content')
    <div class="row">
        <div class="col-lg-4 d-flex grid-margin stretch-card">
            <div class="card bg-primary">
                <div class="card-body text-white">
                    <h3 class="font-weight-bold mb-3">&#x20A6;<span>{{number_format($wallet->wallet_balance, 2)}}</span></h3>
                    <div class="progress mb-3">
                        
                        @if ($wallet->wallet_balance < 2000)
                           <div class="progress-bar  bg-danger" role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        @elseif($wallet->wallet_balance >= 2000)
                            <div class="progress-bar  bg-warning" role="progressbar" style="width: 40%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        @else
                           <div class="progress-bar  bg-success" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        @endif" 
                      
                    </div>
                    <p class="pb-0 mb-0">Wallet Banlance</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 d-flex grid-margin stretch-card">
            <div class="card sale-diffrence-border">
                <div class="card-body">
                    <h2 class="text-dark mb-2 font-weight-bold">&#x20A6;<span>{{number_format(Auth::user()->wallet->daily_earnings, 2)}}</span></h2>
                    
                    <p class="pb-0 mb-0">Daily Earnings</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 d-flex grid-margin stretch-card">
            <div class="card sale-visit-statistics-border">
                <div class="card-body">
                    <h2 class="text-dark mb-2 font-weight-bold">&#x20A6;<span>{{number_format(Auth::user()->withdraw()->sum('amount'), 1)}}</span></h2>
                    <h4 class="card-title mb-2">Last Withdraw</h4>
                    <small class="text-muted">Date: {{\Carbon\Carbon::parse(Auth::user()->wallet()->first()->created_at)->format('Y-m-d')}}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 grid-margin d-flex stretch-card">
                <div class="card">
                <div class="card-body">
                
                <p class="card-description">
                    Top 10 Participants of the morning session
                </p>

                @if ($typings->count() > 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                    <thead>
                        <tr>
                        <th>User</th>
                        <th>Position</th>
                        <th>Errors</th>
                        <th>Words Typed</th>
                        <th>Amount Earned</th>
                        <th>Time (sec)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($typings as $type)
                            <tr>
                                <td>{{$type->username}}</td>
                                <td class="text-success"> {{$type->position}} <i class="mdi mdi-arrow-up"></i></td>
                                <td>{{$type->errors}}</td>
                                <td>{{$type->words_typed}}</td>
                                <td>{{'oi'}}</td>
                                <td>
                                    {{$type->sec}} secs
                                </td>
                            </tr>
                            
                        @endforeach
                        
                    </tbody>
                    </table>
                </div>
                    
                @endif
                
            </div>
            </div>
        </div>
    </div>

@endsection