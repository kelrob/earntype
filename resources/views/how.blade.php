@extends('layouts.main')
@section('title', 'How Earntype Works')

@section('content')
    

        <div id="colorlib-contact">
            
            <div class="container">
                <p class="mb--2">
                    <h4>Whats's Earntype</h4>
                    EarnType is an online Type-based competition platform that pays members daily for typing. 
                    It is an online earning opportunity that suits everyone- professionals, market men women, stay at home moms, 
                    students, old and young, those who can refer and those who can not refer, etc.
                </p>


                <div>
                    <h4>HOW EarnType WORKS</h4>

                    <p>First we have three (3)  sessions.</p>
                    <ol>
                    <li>Morning session</li>
                    <li>Afternoon  Session</li>
                    <li>Evening session.</li>
                    </ol>
                   
                </div>


                <p>
                    For Each session, a member earns N50 as daily activity earnings per session.
                    When you play by TYPING For the 3 sessions, you earn N150 altogether.
                </p>

                - Login your account, Then Click on START TYPING on your dashboard to start typing the ( Sentences or word) that will be displayed for you. <br>


                - Your time starts counting when you click the START TYPING button. <br>


                - Winners are seen on a LEADERBOARD every 12pm for Morning session,  4pm for Afternoon Session and 10pm for evening session. <br>

                -Top 1- 50 members who typed faster will receive N500 immediately to their wallet immediately the result appears on the leaderboard by 12pm,4pm and 10pm Respectively. <br>

                - In addition, EarnType will host a daily Quiz from Monday to Thursday for Free and 3 members will win 2,000 Naira each daily. <br>

                -While From Friday to Sunday, Our Quiz will be a Paid one (@ N50 ). 10 people will win N2,000 each daily. <br>
            </div>
        </div>
@endsection