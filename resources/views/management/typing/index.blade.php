@extends('layouts.management')
@section('title', 'Vendors')

@section('content')
    <div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
        <div class="card shadow">
            <div class="card-header bg-transparent">
                <h3 class="mb-0">Vendors</h3>
                <a href="{{route('management.vendors.add')}}" class="btn btn-primary btn-sm pull-right">Add Vendor</a>
            </div>
            <div class="card-body">
               <div class="col-md-6 offset-md-3">
                        <h4 class="mb-4">Add A New Vendor</h4>
                        <form action="{{route('management.vendors.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Vendor Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Vendor" value="{{old('name')}}" aria-label="Vendor" aria-describedby="basic-addon1">
                                @error('name')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="email" aria-label="email" aria-describedby="basic-addon1">
                                @error('email')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" name="contact_number" value="{{old('contact_number')}}" class="form-control" placeholder="contact_number" aria-label="contact_number" aria-describedby="basic-addon1">
                                @error('contact_number')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Account name</label>
                                <input type="text" name="account_name" value="{{old('account_name')}}" class="form-control" placeholder="account_name" aria-label="account_name" aria-describedby="basic-addon1">
                                @error('account_name')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Account number</label>
                                <input type="text" name="account_number" value="{{old('account_number')}}" class="form-control" placeholder="account_number" aria-label="account_number" aria-describedby="basic-addon1">
                                @error('account_number')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Bank</label>
                                <input type="text" name="bank_name" value="{{old('bank_name')}}" class="form-control" placeholder="bank_name" aria-label="bank_name" aria-describedby="basic-addon1">
                                @error('bank_name')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg mb-3 btn-block">Add Vendor</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection