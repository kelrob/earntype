@extends('layouts.management')
@section('title', 'Add Typing Session')

@section('content')
    <div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
        <div class="card shadow">
            <div class="card-header bg-transparent">
                <h3 class="mb-0">Add Typing Session</h3>
            </div>
            <div class="card-body">
               <div class="col-md-6 offset-md-3">
                        <form action="{{route('management.typing.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Type</label>
                                {{-- <input type="text" name="name" class="form-control" placeholder="Vendor" value="{{old('name')}}" aria-label="Vendor" aria-describedby="basic-addon1"> --}}
                                <select name="type" class="form-control">
                                    <option value="morning">Morning</option>
                                    <option value="afternoon">Afternoon</option>
                                    <option value="evening">Evening</option>
                                </select>
                                @error('type')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>level</label>
                                <select name="level" class="form-control">
                                    <option value="1">Level1</option>
                                    <option value="2">Level2</option>
                                    <option value="3">Level3</option>
                                </select>
                                @error('level')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Text</label>
                               <textarea name="text" cols="30" rows="10" class="form-control"></textarea>
                                @error('text')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        
                            <button type="submit" class="btn btn-primary btn-lg mb-3 btn-block">Add Session</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection