@extends('layouts.management')
@section('content')
    <div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
        <div class="card shadow">
            <div class="card-header bg-transparent">
            <h3 class="mb-0"><span class="text-success">({{count($coupons)}}) </span> Coupon Created Successfully</h3>
            </div>
            <div class="card-body">
                <div class="mt-3 mb-3" style="display: flex; justify-content: right;">
                    <button class="btn btn-sm btn-primary" id="copy" type="button">Copy Coupon Codes</button>
                </div>
               
                @if (count($coupons) > 0)
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Coupon Code</th>
                                <th scope="col">Coupon Vendor</th>
                            </tr>
                            </thead>
                            <tbody>
                                {{-- @for ($i = 0; $i < count($coupons); $i++)
                                <tr>
                                    <th scope="row">
                                        {{$coupons[$i]->code}}
                                    </th>
                                    <td>
                                        {{$coupons[$i]->given_to}}
                                    </td>
                                </tr>
                                    
                                @endfor --}}
                                @foreach($coupons as $coupon)
                                <tr>
                                   <th scope="row">
                                        {{$coupon}}
                                    </th>
                                  
                                </tr>
                                
                                @endforeach
                               
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection