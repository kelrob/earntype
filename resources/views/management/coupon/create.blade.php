@extends('layouts.management')
@section('title', 'Create Coupon')
@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <h3 class="mb-0">Create Coupon</h3>
                </div>
                <div class="card-body">
                    <div class="col-md-6 offset-md-3">
                        <h4 class="mb-4">Create Coupon</h4>
                        <form action="{{route('management.coupon.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Vendor Name</label>
                                 <select name="vendor" class="form-control">
                                    @php
                                        $vendors = App\Vendor::all();
                                    @endphp
                                    @if ($vendors->count() > 0)
                                        @foreach ($vendors as $person)
                                            <option value="{{$person->id}}">{{$person->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {{-- <input type="text" name="vendor" class="form-control" placeholder="Vendor" aria-label="Vendor" aria-describedby="basic-addon1"> --}}
                            </div>

                            <div class="form-group">
                                <label>Amount Of Coupons</label>
                                <input type="number" name="amount" class="form-control" placeholder="amount" aria-label="amount" aria-describedby="basic-addon1">
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg mb-3 btn-block">Generate Coupons</button>
                        </form>

                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection