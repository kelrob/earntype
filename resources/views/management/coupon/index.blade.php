@extends('layouts.management')
@section('title', 'coupons')
@section('content')
    <div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
        <div class="card shadow">
            <div class="card-header bg-transparent">
                <h3 class="mb-0">Coupons</h3>
            </div>
            <div class="card-body">
                <a href="{{route('management.coupon.create')}}" class="btn btn-primary btn-sm">Create Coupon</a>
                @if ($coupons->count() > 0)
                    <div class="table-responsive mt-3">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>

                                    <th scope="col">#</th>
                                    <th scope="col">Code</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Vendor</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                @php
                                    $i= 0;
                                @endphp
                                <tbody>
                                    @foreach ($coupons as $coupon)
                                        <tr>
                                            <th>
                                                {{++$i}}
                                            </th>
                                            <th scope="row">
                                               {{$coupon->code}}
                                            </th>
                                            <td>
                                                @if ($coupon->status == 'used')
                                                    <span class="badge badge-pill badge-primary">{{$coupon->status}}</span>
                                                @else
                                                    <span class="badge badge-pill badge-warning">{{$coupon->status}}</span>
                                                @endif
                                                
                                            </td>
                                            <td>
                                                @if ($coupon->given_to == null)
                                                    
                                                @else
                                                    {{$coupon->given_to}}
                                                @endif
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                
                                </tbody>

                            </table>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-8 px-6 offset-md-2">
                                {{$coupons->links()}}
                            </div>
                        </div>
                    
                @endif


            </div>
        </div>
        </div>
    </div>
    </div>
@endsection