@extends('layouts.management')
@section('title', 'Users')
@section('header', 'Dashboard')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                <h3 class="mb-0">Users</h3>
                </div>
                <div class="card-body">
                    @if ($users->count() > 0)
                        @php
                            $i = 0;
                            $referrals = App\User::where('referrer_id', '!=', null)->count();
                        @endphp

                    <div class="my-4">
                        <strong>Total Referrals: {{$referrals}}</strong>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th class="col">#</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Referrals</th>
                                    <th scope="col">Date Signed In</th>
                                    <th scope="col">Wallet Balance</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <th scope="row">
                                       {{ucfirst($user->username)}}
                                    </th>
                                    <td>
                                        {{$user->email}}
                                    </td>
                                    <td>
                                        {{$user->referrals->count()}}
                                    </td>
                                    <td>
                                       {{\Carbon\Carbon::parse($user->created_at)->diffForHumans()}}
                                    </td>
                                    <td>
                                        <span>&#x20A6;</span> {{number_format($user->wallet->wallet_balance,2)}}
                                    </td>
                                   
                                    
                                </tr>
                                    
                                @endforeach
                            
                            </tbody>
                        </table>
                        </div>

                        <div class="mt-3" style="display: flex; justify-content:center;">
                            {{$users->links()}}
                        </div>
                    @else
                        <div class="mt-6 text-center">
                            <h1>No Users Yet</h1>
                        </div>
                    @endif
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection