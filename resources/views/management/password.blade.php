@extends('layouts.management')
@section('title', 'Password Reset')
@section('header', 'Password Reset')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                <h3 class="mb-0">Password Reset</h3>
                </div>
                <div class="card-body">
                   <div class="col-md-6 offset-md-3">
                       
                        <form action="{{route('management.password.reset')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="name" class="form-control" placeholder="Vendor" value="{{Auth::user()->username}}" aria-label="Vendor" aria-describedby="basic-addon1" readonly>
                                @error('name')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="password" name="old_password" class="form-control" placeholder="Old Password" aria-label="old_password" aria-describedby="basic-addon1">
                                @error('old_password')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="password" name="new_password" class="form-control" placeholder="New Password" aria-label="new_password" aria-describedby="basic-addon1">
                                @error('new_password')
                                    <span class="text-sm mt-2 text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            

                            <button type="submit" class="btn btn-primary btn-lg mb-3 btn-block">Reset Password</button>
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection
