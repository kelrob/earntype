@extends('layouts.management')
@section('title', 'Leader Board')
@section('header', 'Leader Board')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                <h3 class="mb-0">Leader Board</h3>
                </div>
                <div class="card-body">
                    @if ($participants == true && $participants->count() > 0)
                        @php
                            $i = 0;
                        @endphp
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th class="col">#</th>
                                    <th scope="col">User</th>
                                    <th class="col">Position</th>
                                    <th class="col">Errors</th>
                                    <th class="col">Words Typed</th>
                                    <th class="col">Skipped Words</th>
                                    <th class="col">Time (sec)</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($participants as $user)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->position}}</td>
                                    <td>{{$user->errors}}</td>
                                    <td>{{$user->words_typed}}</td>
                                    <td>{{$user->skipped_words}}</td>
                                    <td>
                                        {{$type->sec}} secs
                                    </td>
                                </tr>
                                    
                                @endforeach
                            
                            </tbody>
                        </table>
                        </div>

                        <div class="mt-3" style="display: flex; justify-content:center;">
                            {{$participants->links()}}
                        </div>
                    @else
                        <div class="mt-6 text-center">
                            <h1>Leader Board is Empty</h1>
                        </div>
                    @endif
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection