@extends('layouts.management')
@section('title', 'Vendors')

@section('content')
    <div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
        <div class="card shadow">
            <div class="card-header bg-transparent">
                <h3 class="mb-0">Vendors</h3>
                <a href="{{route('management.vendors.add')}}" class="btn btn-primary btn-sm pull-right">Add Vendor</a>
            </div>
            <div class="card-body">
                @if ($vendors->count() > 0)
                @php
                    $i = 0;
                @endphp
                    <div class="table-responsive">
                         <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th class="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone 1</th>
                                    <th scope="col">Phone (whatsapp)</th>
                                    <th scope="col">Coupon Amount</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($vendors as $vendor)
                                    <tr>
                                        <td>{{++$i}}</td>
                                        <td>{{$vendor->name}}</td>
                                        <td>{{$vendor->email}}</td>
                                        <td>{{$vendor->contact_no}}</td>
                                        <td>{{$vendor->whatsapp}}</td>
                                        <td>{{$vendor->coupons()->count()}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    
                @endif
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection