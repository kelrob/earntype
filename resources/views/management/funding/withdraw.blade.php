@extends('layouts.management')
@section('title', 'Withdraw requests')
@section('header', 'Dashboard')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <h3 class="mb-0">Pending Withdrawal Request</h3>
                </div>
              
                <div class="card-body">
                  <h4 class="my-3">Total amount: &#8358;{{$pendingWithdraw->sum('amount')}}</h4>
                    <div class="my-3">
                        <a href="{{route('management.payOut.all')}}" class="btn btn-primary btn-sm">CheckOut All Requests</a>
                    </div>
                    @if ($pendingWithdraw->count() > 0)
                        @php
                            $i = 0;
                        @endphp
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th class="col">#</th>
                                    <th scope="col">User</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Account Number</th>
                                    <th scope="col">Account Name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pendingWithdraw as $request)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <th scope="row">
                                       {{$request->user->username}}
                                    </th>
                                    <td>
                                      <span>&#x20A6;</span> {{number_format($request->amount,2)}}
                                    </td>
                                    <td>
                                       {{$request->account_number}}
                                    </td>
                                    <td>
                                        {{$request->account_name}}
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($request->created_at)->diffForHumans()}}
                                    </td>
                                    <td>
                                        @if ($request->status == 0)
                                          <a href="{{route('management.payOut', $request->user_id)}}" class="btn btn-primary btn-sm">CheckOut</a>
                                        @else
                                          <b>Paid Out</b>
                                        @endif
                                    </td>
                                    
                                </tr>
                                    
                                @endforeach
                            
                            </tbody>
                        </table>
                        </div>

                        <div class="mt-3" style="display: flex; justify-content:center;">
                            {{$pendingWithdraw->links()}}
                        </div>
                    @else
                        <div class="mt-6 text-center">
                            <h1>No Pending Withdrawal Request Yet</h1>
                        </div>
                    @endif
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection
