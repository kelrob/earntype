@extends('layouts.management')
@section('title', 'Add Funds')
@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <h3 class="mb-0">Add Funds</h3>
                </div>
                <div class="card-body">
                    <div class="col-md-6 offset-md-3">
                        
                        <form action="{{route('management.fund')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="number" name="amount" class="form-control" placeholder="amount" aria-label="Vendor" aria-describedby="basic-addon1">
                            </div>

                            <div class="form-group">
                                <label>Days</label>
                                <input type="number" name="daysfor" class="form-control" placeholder="daysfor" aria-label="amount" aria-describedby="basic-addon1">
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg mb-3 btn-block">Add Funds</button>
                        </form>

                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection