@extends('layouts.management')
@section('title', 'Funds Statistics')
@section('header', 'Funds Satistics')

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <h3 class="mb-0">Funds Management</h3>
                </div>
                <div class="card-body">
                   <div class="container">
                       @if ($funds == true)
                       
                        <div class="row">
                            <div class="col-sm">
                                <div class="card card-stats">
                                    <!-- Card body -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <h5 class="card-title text-uppercase text-muted mb-0">Amount Remains</h5>
                                                <span class="h2 font-weight-bold mb-0"><span class="text-sm">&#x20A6;</span>{{number_format($funds->amount - $funds->fund_remains, 2)}}</span>
                                            </div>
                                            <div class="col-auto">
                                            <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                                                <i class="ni ni-chart-pie-35"></i>
                                            </div>
                                            </div>
                                        </div>
                                        <p class="mt-3 mb-0 text-sm">
                                            <span class="text-nowrap">From: <span class="text-sm">&#x20A6;</span>{{number_format($funds->amount, 2)}}</span>
                                        </p>
                                    </div> 
                                </div>
                            </div>
                            
                            <div class="col-sm">
                                <div class="card card-stats">
                                    <!-- Card body -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <h5 class="card-title text-uppercase text-muted mb-0">End Date</h5>
                                                <span class="h2 font-weight-bold mb-0">{{Carbon\Carbon::parse($funds->end_date)->format('d-M-Y')}}</span>
                                            </div>
                                            <div class="col-auto">
                                            <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                                <i class="ni ni-chart-pie-35"></i>
                                            </div>
                                            </div>
                                        </div>
                                        <p class="mt-3 mb-0 text-sm">
                                            <span class="text-nowrap">Added: {{Carbon\Carbon::parse($funds->created_at)->diffForHumans()}}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="card card-stats">
                                    <!-- Card body -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <h5 class="card-title text-uppercase text-muted mb-0">Number of Days</h5>
                                                <span class="h2 font-weight-bold mb-0">{{$funds->days}}Days</span>
                                            </div>
                                            <div class="col-auto">
                                            <div class="icon icon-shape bg-orange text-white rounded-circle shadow">
                                                <i class="ni ni-chart-pie-35"></i>
                                            </div>
                                            </div>
                                        </div>
                                        <p class="mt-3 mb-0 text-sm">
                                            <span class="text-nowrap">{{Carbon\Carbon::now()->diffInDays($funds->end_date, false)}}Days To Go</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-sm col-md-4">
                                <div class="card card-stats">
                                    <!-- Card body -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <h5 class="card-title text-uppercase text-muted mb-0">Beneficiaries</h5>
                                                <span class="h2 font-weight-bold mb-0">{{$funds->users_to_award}} Users</span>
                                            </div>
                                            <div class="col-auto">
                                            <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                                                <i class="ni ni-chart-pie-35"></i>
                                            </div>
                                            </div>
                                        </div>
                                        {{-- <p class="mt-3 mb-0 text-sm">
                                            <span class="text-nowrap">{{Carbon\Carbon::now()->diffInDays($funds->end_date, false)}}Days To Go</span>
                                        </p> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                       @endif


                        <div class="my-4">
                            <a href="{{route('management.addfunds')}}" class="btn btn-primary btn-md">Add Funds</a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection