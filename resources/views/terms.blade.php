@extends('layouts.main')
@section('title', 'Disclaimer')
@section('content')

<div id="colorlib-contact">
            
            <div class="container">
                <div class="terms mb--2">
                    <h2>Terms of service</h2>
                    <h4>EarnType Terms of Service.</h4>

                    <p class="mb--2">
                        <em>Introduction</em>
                            The following Terms and Conditions control your membership on EarnType . You agree that you have read 
                            and understand this Agreement (“Agreement”) and that your membership on EarnType  shall be subject to the following 
                            Terms and Conditions between you (the “Member”) and EarnType.com
                            These Terms and Conditions may be modified at any time by EarnType Administrators without notice. Please review them 
                            from time to time since your ongoing use is subject to the terms and conditions as modified. Your continued participation in 
                            EarnType.com after such modification shall 
                            be deemed to be your acceptance of any such modification. If you do not agree to these Terms and Conditions, please do 
                            not register on EarnType.com
                    </p>

                    <p class="mb--2">
                        <h4>Terms of Participation</h4>
                        Member must be 18 years of age or older to participate. Members must provide EarnType with accurate, complete and updated 
                        registration information, including but not 
                        limited to an accurate email address, phone number and bank account details.
                        To the full extent allowed by applicable law, EarnType at its sole discretion and for any or no reason may refuse to accept 
                        applications for membership.
                    </p>

                    <p class="mb--2">
                        <h4>Members may not</h4>

                        <p><span>(i) </span> activate or use more than one Member account;</p>
                        <p><span>(ii)</span> select or use an Email Address of another person;</p>
                        <p><span>(iii)</span> use a false or misleading name (Except for privacy), mailing address, or email address to activate or use a Member account.</p>

                        <p class="mt--2">
                            By signing up for the EarnType, member is opting-in to receive other special offer emails. If you do not wish to receive these emails, you may cancel your account anytime.
                            Member agrees not to abuse his or her membership privileges by acting in a manner inconsistent with this Agreement.
                            Member agrees not to attempt to earn through other means than the legitimate channels authorized by EarnType.
                            Member agrees not to participate in any fraudulent behavior of any kind.
                        </p>
                    </p>
                    <p class="mb--2">
                        <h4>Refund Policy:</h4>
                        As we are offering non-tangible virtual digital goods  
                        which is form of registration fee , we do not generally issue refunds after the purchase 
                        of coupon code to use EarnType.com . Please note that by purchasing the EarnType Coupon code, you 
                        agree to the no Refund Policy.
                    </p>
                    <p class="mb--2">
                        <h4>Membership activities, sponsored post sharing:</h4>
                        Members approve sponsored post of other members. Post with Less Done, won't receive their Daily Activity Earning.
                        Sponsored post sharing must correspond with the post date and must be shared as recommended.
                    </p>
                    <p class="mb--2">
                        <h4>Payment:</h4>
                        We pay out Daily for referrals and winners within the Top 1 - 50 Leaderboard, while daily activities earning 
                        is paid Monthly. we may choose to suspend the Daily Activity Earning at any time , for the profitablity of the 
                        business operation to keep runinng.  
                    </p>
                </div>
                <div class="disclaimer">
                    <h3>Disclaimers:</h3>
                    <div>
                        <p class="mb--2">
                            MEMBER EXPRESSLY AGREES THAT USE OF THE SERVICE IS AT MEMBER’S SOLE RISK. THE SERVICE IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. 
                            TO THE MAXIMUM EXTENT ALLOWED BY APPLICABLE NIGERIAN LAW, EARNTYPE EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, EXPRESS OR 
                            IMPLIED BY LAW, CUSTOM OR OTHERWISE, INCLUDING WITHOUT LIMITATION ANY WARRANTY OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A 
                            PARTICULAR PURPOSE OR NON-INFRINGEMENT. EARNTYPE MAKES NO WARRANTY REGARDING ANY GOODS OR SERVICES PURCHASED OR OBTAINED THROUGH THE PROGRAM 
                            OR ANY TRANSACTIONS ENTERED INTO THROUGH THE PROGRAM.
                        </p>
                        <p class="mb--2">
                            TO THE MAXIMUM EXTENT ALLOWED BY APPLICABLE NIGERIAN LAW, NEITHER EARNTPE NOR ANY OF ITS MEMBERS, SUBSIDIARIES, 
                            PUBLISHERS, SERVICE PROVIDERS, LICENSORS, OFFICERS, DIRECTORS OR EMPLOYEES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
                            SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR RELATING TO THIS AGREEMENT, RESULTING FROM THE USE OR THE INABILITY TO USE 
                            THE SERVICE OR FOR THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES OR RESULTING FROM ANY GOODS OR SERVICES PURCHASED OR 
                            OBTAINED OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO THROUGH THE PROGRAM OR RESULTING FROM UNAUTHORIZED ACCESS TO OR ALTERATION 
                            OF USER’S TRANSMISSIONS OR DATA, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, USE, 
                            DATA OR OTHER INTANGIBLE, EVEN IF SUCH PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
                        </p>

                        <p class="mb--2">
                            To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, EarnType uses 
                            appropriate industry standard procedures to safeguard the confidentiality of Member’s personal information, such as SSL, 
                            firewall, encryption, token authentication, application proxies, monitoring technology, and adaptive analysis of the Website’s 
                            traffic to track abuse of the EARNTYPE Website and its data. However, no data transmitted over the Internet can be 100% secure. 
                            As a result, while EarnType strives to protect its Members personal information, EarnType cannot guarantee the security of any information 
                            that Members transmit to or from the participating advertisers/merchants and Member does so at his/her own risk.
                        </p>
                    </div>
                    <div class="earning">
                        <h4>Earnings Disclaimer:</h4>
                        <p class=" mb--4">
                            
                            While we make every effort to ensure that we accurately represent all the products and services reviewed on EarnType and their potential for income, it should be noted that earnings and income statements made by EarnType and its advertisers / sponsors are estimates only of what we think you can possibly earn. There is no guarantee that you will make these levels of income and you accept the risk that the earnings and income statements differ by individual.

                            As with any business, your results may vary, and will be based on your individual capacity, business experience, expertise, and level of desire. There are no guarantees concerning the level of success you may experience. The testimonials and examples used are exceptional results, which do not apply to the average purchaser, and are not intended to represent or guarantee that anyone will achieve the same or similar results. Each individual’s success depends on his or her background, dedication, desire and motivation.
                        </p>
                            There is no assurance that examples of past earnings can be duplicated in the future. We cannot guarantee your future results and/or success. There are some unknown risks in business and on the internet that we cannot foresee which could reduce results you experience. We are not responsible for your actions.
                            The use of our information, products and services should be based on your own due diligence and you agree that EarnType and the advertisers / sponsors of this website are not liable for any success or failure of your business that is directly or indirectly related to the purchase and use of our information, products and services reviewed or advertised on this website.



                            <p class="mb--4">
                                All the information on this website – <a target="_self" href="https://earntype.com">earntype.com</a> – is published in good faith and for general information purpose only. <a href="https://earntype.com">earntype.com</a> does not make any warranties about the completeness, reliability and accuracy of this information. Any action you take upon the information you find on this website ( <a href="https://earntype.com">earntype.com</a> ), is strictly at your own risk. <a target="_self" href="https://earntype.com">earntype.com</a> will not be liable for any losses and/or damages in connection with the use of our website.

                                Please be also aware that when you leave our website, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their “Terms and Condition” before engaging in any business or uploading any information.
                            </p>

                            <p class="mb--3">
                                <h5>Consent</h5>
                                By using our website, you hereby consent to our disclaimer and agree to our terms.
                            </p>


                            <p>
                                    <h5>Update</h5>
                                Should we update, amend or make any changes to this document, those changes will be prominently posted here.
                            </p>
                        </p>
                    </div>
                    <div>
                        <p class="mt--2" style="background-color: rgb(21,195,154); color: #ffff5f; padding:5px 10px;">
                            This Agreement constitutes the entire Agreement between Member and EarnType in connection with 
                            general membership regarding the subject matter contained herein. If any provision of this AGREEMENT is 
                            found invalid or unenforceable, that provision will be enforced to the maximum extent permissible, and the 
                            other provisions of this AGREEMENT will remain in force. Failure of either party to exercise or enforce any 
                            of its rights under this AGREEMENT, within two(2) months the cause arose, will act as a waiver of such rights. 
                            In the event of any dispute or need for interpretation or enforcement of terms, arising out of this agreement, 
                            parties shall refer to arbitration before litigation.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    
@endsection