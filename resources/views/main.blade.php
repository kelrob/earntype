@extends('layouts.main')
@section('title', 'Earn Money while typing in just 2 minutes')

@section('content')
    <section id="home" class="video-hero" style="height: 700px; background-image: url(images/cover_img_1.jpg);  background-size:cover; background-position: center center;background-attachment:fixed;" data-section="home">
        <div class="overlay"></div>
            <!-- <a class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=vqqt5p0q-eU',containment:'#home', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, quality:'default'}"></a>  -->
            <div class="display-t text-center">
                <div class="display-tc">
                    <div class="container">
                        <div class="col-md-12 col-md-offset-0">
                            <div class="animate-box">
                                <h2>A Reliable way to Earn Online, Typing</h2>
                                
                                <p>
									<a href="{{route('dashboard')}}" class="btn btn-primary btn-lg btn-custom">Get Started</a> 
                                    <a href="/playdemo" class="btn btn-primary btn-lg btn-custom">Play Demo</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


        <div class="colorlib-services colorlib-bg-white" id="home">
			<div class="container">
				<div class="row">
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="fas fa-sync-alt"></i>
							</span>
							<div class="desc">
								<h3>Reliable</h3>
								<p>
									You are in a trustworthy platform that guarantees you 24/7 consistent system operation. No panic, we are in this for the Long term.
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="fas fa-wallet"></i>
							</span>
							<div class="desc">
								<h3>Fast payout</h3>
								<p>You receive your earning in less than 24 hours to your local bank account. No stories.....receive payment in speed.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="fas fa-user-shield"></i>
							</span>
							<div class="desc">
								<h3>Safe</h3>
								<p>Your earnings are assured and well secured on EarnType. </p>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        
        <div class="colorlib-intro" id="how">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
						<h2>How It Works</h2>
						<p>
							Login and Type Faster to lead on our Daily Leaderboard. Earn N500 Instantly with free N150 daily activity earning when you participate.
							(check file attachment two- on ways it should appear on earntype site)
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 animate-box" style="display: flex; justify-content: center;">
						{{-- <span class="play"><a href="https://youtube.com/A9bS_7gxZHc" class="pulse popup-vimeo"><i class="fas fa-play"></i></a> </span>--}}
						
							<iframe width="560" height="320" src="https://www.youtube.com/embed/A9bS_7gxZHc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
		<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(images/cover_img_1.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4 col-sm-4 text-center animate-box">
							<div class="counter-entry">
								<span class="icon"><i class="flaticon-ribbon"></i></span>
								<div class="desc">
									<span class="colorlib-counter js-counter" data-from="0" data-to="5000" data-speed="5000" data-refresh-interval="10"></span>
									<span class="colorlib-counter-label">Members Trust and Use EarnType Every day.</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 text-center animate-box">
							<div class="counter-entry">
								<span class="icon"><i class="flaticon-church"></i></span>
								<div class="desc">
									<span class="colorlib-counter js-counter" data-from="0" data-to="250" data-speed="5000" data-refresh-interval="10"></span>
									<span class="colorlib-counter-label">Content Typed by over 5,000 members</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 text-center animate-box">
							<div class="counter-entry">
								<span class="icon"><i class="flaticon-dove"></i></span>
								<div class="desc">
									<span class="colorlib-counter js-counter" data-from="0" data-to="150" data-speed="5000" data-refresh-interval="10"></span>
									<span class="colorlib-counter-label">Million Naira Payout to Trusted and Reliable Members.</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        

        {{-- <div class="colorlib-blog" id="vendors">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
						<h2>Our Coupon Vendors</h2>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 animate-box">
						<article>
							<h2>Building the Mention Sales Application on Unapp</h2>
							<p class="admin"><span>May 12, 2018</span></p>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
							<p class="author-wrap"><a href="#" class="author-img" style="background-image: url(images/person1.jpg);"></a> <a href="#" class="author">by Dave Miller</a></p>
						</article>
					</div>
					<div class="col-md-4 animate-box">
						<article>
							<h2>Building the Mention Sales Application on Unapp</h2>
							<p class="admin"><span>May 12, 2018</span></p>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
							<p class="author-wrap"><a href="#" class="author-img" style="background-image: url(images/person2.jpg);"></a> <a href="#" class="author">by Dave Miller</a></p>
						</article>
					</div>
					<div class="col-md-4 animate-box">
						<article>
							<h2>Building the Mention Sales Application on Unapp</h2>
							<p class="admin"><span>May 12, 2018</span></p>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
							<p class="author-wrap"><a href="#" class="author-img" style="background-image: url(images/person3.jpg);"></a> <a href="#" class="author">by Dave Miller</a></p>
						</article>
					</div>
				</div>
				<p class="text-center"><a href="#" class="btn btn-primary btn-outline with-arrow">Show More<i class="icon-arrow-right3"></i></a></p>
			</div>
        </div>
         --}}

        		<div class="colorlib-work-featured colorlib-bg-white" id="faqs">
			<div class="container">
				<div class="row mobile-wrap">
					<div class="col-md-5 animate-box">
						<div class="mobile-img" style="background-image: url(/main/images/question.jpg);">
						
						</div>
					</div>
					<div class="col-md-7 animate-box">
						<div class="desc">
							<h2>Frequently Asked Questions</h2>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>What's the cost of registration?</h4>
									<p>
										1,600naira only
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>Will I make any payment again after my registration?</h4>
									<p>
										NO!
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>When is the payout date?</h4>
									<p>
										 Payout is done on a daily basis. You can withdraw all your profit at any time.
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>What is the minimum payout?</h4>
									<p>
										 The minimum amount you can withdraw is #500 when you win, anytime anyday and you will receive your payment within 24 hours
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>In what way can I withdraw my earnings?</h4>
									<p>
										You can only withdraw directly to you bank account.
									</p>
								</div>
							</div>
							<p>
								<a href="{{route('faq')}}" class="btn btn-primary btn-outline with-arrow">
									More Answers
									<i class="fas fa-chevron-right"></i>

								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection