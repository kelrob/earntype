@extends('layouts.main')
@section('title', 'Vendors')

<style>
    .vendor-section{
        display: flex;
        flex-direction: column;
        padding: 20px 0;
        margin-bottom: 30px;
        text-align: center;
        justify-content: center;
        border: 1px solid #22ddee;
        margin: 0 auto;
        width: 100%;
    }.links{
        overflow-x: auto;
        display: flex;
        margin-top: 20px;
        justify-content: center;

    }

</style>
@section('content')
    

        <div id="colorlib-contact">
            
            <div class="container">
                <div class="work-flex">
                    <h1>Our Coupon Vendors</h1>
                    @php
                        $vendors = App\Vendor::inRandomOrder()->get();
                    @endphp
                    @foreach ($vendors as $vendor)
                    <div class="vendor-section">
                        <div class="text-center text-sm text-bold">
                            <strong>{{$vendor->name}}</strong>
                        </div>
                        <div class="link">
                         <span>Contact Vendor: </span><a class="btn" href="https://{{$vendor->whatsapp}}">{{$vendor->contact_no}}</a>
                        </div>
                        
                    </div>
                        
                    @endforeach

                    
                </div>
            </div>
        </div>
@endsection