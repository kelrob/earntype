@extends('layouts.auth')
@section('title', 'Create an Account')

@section('content')


<!-- Page content -->
    <div class="container mt--8 pb-5">
      <!-- Table -->
      <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
          <div class="card bg-secondary shadow border-0">
           
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <strong>Register</strong>
              </div>
                <form role="form" action="{{route('register')}}" method="POST">
                  @csrf

                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                    </div>
                    <input class="@error('username') border-bottom border-danger @enderror form-control" placeholder="Username" name="username" value="{{old('username')}}" type="text" required autocomplete="username" autofocus>
                  </div>

                   @error('username')
                        <span class=" text-xs text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="@error('email') border-bottom border-danger @enderror form-control" placeholder="Email" type="email" value="{{old('email')}}" name="email" required autocomplete="email" autofocus>
                  </div>
                   @error('email')
                        <span class=" text-xs text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-spaceship"></i></span>
                    </div>
                    <input class="@error('coupon') border-bottom border-danger @enderror form-control" placeholder="Coupon" type="text" name="coupon" required autocomplete="coupon" autofocus>
                  </div>

                   @error('coupon')
                        <span class=" text-xs text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="@error('password') border-bottom border-danger @enderror form-control" placeholder="Password" name="password" type="password" autofocus autocomplete="new-password">
                  </div>

                   @error('password')
                        <span class=" text-xs text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="row my-4">
                  <div class="col-12">
                    <div class="custom-control custom-control-alternative custom-checkbox">
                      <input class="custom-control-input" name="agree" value="1" id="customCheckRegister" type="checkbox">
                      <label class="custom-control-label" for="customCheckRegister">
                        <span class="text-muted">I agree with the <a href="{{route('terms')}}">Terms</a>, <a href="{{route('policy')}}">Data Policy</a></span>
                      </label>
                    </div>

                     @error('agree')
                          <span class=" text-xs text-danger" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>


                <div class="text-center">
                  <button type="submit" class="btn btn-primary mt-4">Create account</button>
                </div>

              </form>
              <div class="text-center mt-2">
                <a href="/vendors" class="text-primary text-bold"><strong>Buy Coupon code here</strong></a>
              </div>
            </div>
          </div>

          <div class="row mt-3">
           
            <div class="col-12 text-center">
                <a href="{{route('login')}}" class="text-light"><small>Login to an existing account</small></a>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  </div>


@endsection