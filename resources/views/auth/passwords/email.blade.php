

@extends('layouts.auth')
@section('title', 'Login')

@section('first')
  Password Reset 
@endsection
@section('content')

    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                  <strong>Reset Password</strong>
              </div>
              @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

            <form action="{{ route('password.email') }}" method="POST">
                @csrf
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control @error('email') border-bottom border-danger @enderror" placeholder="Email" name="email" value="{{old('email')}}" type="email" autofocus>
                  </div>
                  @error('email')
                        <span class="text-danger text-sm" role="alert">
                            <small><strong>{{ $message }}</strong></small>
                        </span>
                    @enderror
                </div>

                

                

                <div class="text-center">
                  <button type="submit" class="btn btn-primary my-4">Send Password Reset Link</button>
                </div>
              </form>
            </div>
          </div>
          
        </div>
      </div>
    </div>

@endsection

