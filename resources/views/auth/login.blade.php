@extends('layouts.auth')
@section('title', 'Login')

@section('content')

    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                  <strong>Sign In</strong>
              </div>

            <form action="{{route('login')}}" method="POST">
                @csrf
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control @error('email') border-bottom border-danger @enderror" placeholder="Email" name="email" value="{{old('email')}}" type="email" autofocus>
                  </div>
                  @error('email')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control  @error('password') border-bottom border-danger @enderror" name="password" placeholder="Password" type="password" required autocomplete="current-password">
                  </div>
                  @error('password')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input" id=" customCheckLogin" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} type="checkbox">
                    <label class="custom-control-label" for=" customCheckLogin">
                        <span class="text-muted">Remember me</span>
                    </label>
                </div>

                <div class="text-center">
                  <button type="submit" class="btn btn-primary my-4">Sign in</button>
                </div>
              </form>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col-6">
                 @if (Route::has('password.request'))
                    <a class="text-light"  href="{{ route('password.request') }}"><small>Forgot password?</small></a>
                @endif
            </div>
            <div class="col-6 text-right">
                <a href="{{route('register')}}" class="text-light"><small>Create new account</small></a>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
