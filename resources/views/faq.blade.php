@extends('layouts.main')
@section('title', 'Play Demo')
<style>
    .features{
        display: flex;
    }
    .features span{
        margin-right: 5px;
    }
</style>
@section('content')
    

        <div id="colorlib-contact">
            
            <div class="container">
                <div class="row mobile-wrap">
					
					<div class="col-md-7 animate-box">
						<div class="desc">
							<h2>Frequently Asked Questions</h2>
							<div class="features">
								<span class="icon">
                                    <i class="far fa-question-circle"></i>
                                </span>
								<div class="f-desc">
                                    <h4>What's the cost of registration?</h4>
									<p>
										1,600naira only
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>Will I make any payment again after my registration?</h4>
									<p>
										NO!
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>When is the payout date?</h4>
									<p>
										 Payout is done on a daily basis. You can withdraw all your profit at any time.
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>What is the minimum payout?</h4>
									<p>
										 The minimum amount you can withdraw is #500 when you win, anytime anyday and you will receive your payment within 24 hours
									</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>In what way can I withdraw my earnings?</h4>
									<p>
										You can only withdraw directly to you bank account.
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>Is multiple accounts allowed?</h4>
									<p>
										No, That is not allowed.
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>What is the Minimum Referral Amount I can withdraw?</h4>
									<p>
										The Minimum Referral amount you can withdraw is N3,000
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>How much do I make if I refer people?</h4>
									<p>
										N1,000 per person you refer.
									</p>
								</div>
                            </div>
                            
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>Must I refer before I can withdraw?</h4>
									<p>
										No. You can withdraw all your money without referral. REFERRAL IS NOT COMPULSORY.
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>What is the Minimum Daily Activity Earning I can withdraw (N150)?</h4>
									<p>
										N4,000 is the minimum daily activity earning you can withdraw.
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>When can i withdraw my Daily Activity Earning (N150)?</h4>
									<p>
										Monthly - 31st of every month.
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4>Is there a daily sponsored post?</h4>
									<p>
										Yes
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4> Must I share daily sponsored post?</h4>
									<p>
										YES. you must share it for you to receive your DAILY ACTIVITY EARNING every end of Month.
									</p>
								</div>
                            </div>
                            <div class="features">
								<span class="icon"><i class="far fa-question-circle"></i></span>
								<div class="f-desc">
									<h4> What will happen if I don't have Upto N4,000 at the end of the month on my daily activity earnings?</h4>
									<p>
										Your earnings will be cleared off and you restart again in a new month.
									</p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
            </div>
        </div>
@endsection