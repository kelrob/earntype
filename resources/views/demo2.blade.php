@extends('layouts.main')
@section('title', 'Play Demo')
@section('added')
    <style>
         ul.countdown {
            list-style: none;
            margin: 15px 15px;
            padding: 0;
            display: block;
            text-align: center;
            }

            ul.countdown li {
            display: inline-block;
            }

            ul.countdown li span {
            font-size: 80px;
            font-weight: 300;
            line-height: 80px;
            }

            ul.countdown li.seperator {
            font-size: 80px;
            line-height: 70px;
            vertical-align: top;
            }

            ul.countdown li p {
            color: #a7abb1;
            font-size: 14px;
            }
    </style>
@endsection
@section('content')
    

        <div id="colorlib-contact">
            
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-8 col-md-offset-6 col-md-pull-4 animate-box">
                        <h2>Play Demo</h2>
                        <form action="#" autocomplete="off">
                            <div id="game">
                                <div class="jumbotron" name="timerContainer" style="padding: 1;">
                                    <center>
                                      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                                      <b id="timer"><span id="minutes"></span>:<span id="seconds"></span></b>
                                        <span name="timerContainer">
                                            <p class="text-center"><b id="text">{{ $game->string }}</b></p>
                                        </span>
                                    </center>
                                </div>
                                
                                <div class="row form-group" >
                                    <div class="col-lg-12">
                                      {{-- <p class="text-center"><b>{{ $game->string }}</b></p> --}}
                                    </div>
                                    <div class="col-md-12">
                                      <div id="error-message" style="display: none">
                                        <div class="alert alert-danger">Texts does not match</div>
                                      </div>
                                      <div id="success-message" style="display: none">
                                        <div class="alert alert-success">Submitted to DB</div>
                                      </div>
                                        <div>
                                            Start Typing
                                        </div>
                                        <textarea name="message" id="textarea" onkeydown="hideError()" cols="30" rows="1" class="form-control"
                                           oncopy="return false" onpaste="return false" style="resize: none;"></textarea>
                                    </div>
                                  <p style="display: none;" id="token">{{ time() }}</p>
                                    <div class="col-md-12" style="margin-top: 2%;" align="center">
                                      <a onclick="processType()" class="btn btn-success" id="btn-submit" style="width: 50%;">Submit</a>
                                    </div>
                                </div>
                                
                            </div>

                            {{-- <div class="form-group">
                                <input  type="submit" value="" class="btn btn-primary">
                            </div> --}}
                        </form>

                        <script>
                          const hideError = () => {
                            $('#error-message').hide();
                          }
                          const processType = () => {
                            let actualText = $('#text').html();
                            let textEntered = $('#textarea').val();
                            if (textEntered === '') {
                              textEntered = 'null'
                            }
                            $.ajax({
                              headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              },
                              data: { 
                                "actualText": actualText,
                                "textEntered": textEntered,
                                "token_t": {{ time() }},
                              },
                              type: "POST",
                              url: `record-session`,
                              beforeSend: function () {
                                
                              },
                              success: function (data) {
                                if (data == 'wrong') {
                                  $('#error-message').show();
                                }

                                if (data == 'correct') {
                                  $('#success-message').show();
                                  $('#timer').hide();
                                  $('#btn-submit').hide();

                                }

                                console.log(data);
                              }
                            });
                          }

                          var sec = 0;
                          function pad ( val ) { return val > 9 ? val : "0" + val; }
                          setInterval( function(){
                              $("#seconds").html(pad(++sec%60));
                              $("#minutes").html(pad(parseInt(sec/60,10)));
                          }, 1000)
                        </script>                            

                    </div>
                </div>
            </div>
        </div>
@endsection

@section('addedB')
    {{-- <script>
        $(document).ready(function () {

            $('#game').hide();
            $('#continue').click(function(){
                $('#game').show();
                $('#info').hide();
            })

        });
    </script> --}}
@endsection